package be.ehb.facturatie.XML;



import be.ehb.facturatie.logic.error.ErrorSender;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public enum XSDEnum {

    USER_CREATE("xsd/user/user.xsd"),
    USER_UPDATE("xsd/user/user.xsd"),
    USER_DELETE("xsd/user/user.xsd"),
    HEARTBEAT("xsd/user/heartbeat.xsd"),
    UUID("xsd/user/UUID.xsd"),
    INVOICE_SENDER("xsd/user/invoice_sender.xsd"),
    RESPONSE("xsd/user/response.xsd"),
    ERROR("xsd/user/error.xsd");


    private final String path;
    private String xsd = null;

    XSDEnum(String path) {
        this.path = path;

        try (final InputStream inputStream = XMLValidator.class.getClassLoader().getResourceAsStream(path);
             final InputStreamReader streamReader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
             final BufferedReader reader = new BufferedReader(streamReader)) {
            final StringBuilder builder = new StringBuilder();

            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }

            xsd = builder.toString();
        } catch (IOException e) {
            ErrorSender.sendError(e.getMessage(),"error");
            e.printStackTrace();
        }
    }

    public String getXsd() {
        return xsd;
    }
}
