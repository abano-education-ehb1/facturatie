package be.ehb.facturatie.XML;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class XMLValueCatcher {
    public String catchValue(String tagName, Document doc) {
        NodeList list = doc.getElementsByTagName(tagName);
        if (list != null && list.getLength() > 0) {
            NodeList subList = list.item(0).getChildNodes();

            if (subList != null && subList.getLength() > 0) {
                //Print the value of the file with tag firstname
                return subList.item(0).getNodeValue();
            }
        }
        return null;
    }

    public NodeList catchList(String tagName, Document doc) {
        NodeList list = doc.getElementsByTagName(tagName);
        if (list != null && list.getLength() > 0) {
            return list.item(0).getChildNodes();
            }
        return null;
        }

    public static Node getNode(Node node, String path) {
        final String[] split = path.split("\\.");
        for (int i = 0; i < node.getChildNodes().getLength(); i++) {
            final Node item = node.getChildNodes().item(i);
            if (item.getNodeName().equalsIgnoreCase(split[0])) {
                if (split.length > 1)
                    return getNode(item, Arrays.stream(split).skip(1).collect(Collectors.joining(".")));
                return item;
            }
        }
        return null;
    }
    public static String getValue(Node node, String path) {
        final String[] split = path.split("\\.");
        for (int i = 0; i < node.getChildNodes().getLength(); i++) {
            final Node item = node.getChildNodes().item(i);
            if (item.getNodeName().equalsIgnoreCase(split[0])) {
                if (split.length > 1)
                    return getValue(item, Arrays.stream(split).skip(1).collect(Collectors.joining(".")));
                return item.getChildNodes().item(0).getNodeValue();
            }
        }
        return null;
    }

}
