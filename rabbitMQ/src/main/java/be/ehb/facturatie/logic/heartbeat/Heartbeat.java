package be.ehb.facturatie.logic.heartbeat;

import be.ehb.facturatie.XML.XMLValidator;
import be.ehb.facturatie.XML.XSDEnum;
import be.ehb.facturatie.logic.error.ErrorSender;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;


import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.TimeoutException;

public class Heartbeat {
    private final static String QUEUE_NAME = "monitoring";

    public  void heartbeat() {

        //create the connection to rabbitMQ
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("10.3.56.3");
        factory.setPort(5672);
        factory.setUsername("integration_project22");
        factory.setPassword("3jPqsaCUBXgHHLzM");

        //Try if the connection is good
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {

            //Assigne the queue to the channel
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);

           //While loop that will send every 3 seconds a message
            while (true) {

                //Create a date format to send later in the message + Get the time now
                //DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
                Date now = new Date();
                long unixTime = Instant.now().getEpochSecond();
                //Making our message
                String message =
                        "<heartbeat>" +
                                "<source>facturatie</source>"  +
                                "<date>" + unixTime  + "</date>" +
                                "</heartbeat>";
                long millis = System.currentTimeMillis();

                //Checking if our format is good of the xml
                if (XMLValidator.validate(XSDEnum.HEARTBEAT, message)) {

                    //If it is succesfull than it will be send to the que of monitoring
                    //System.out.println("----The format is good and will be send to monitoring----");
                    channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
                    //System.out.println(" [x] Sent '" + message + "'");
                    //Wait 3 seconds before sending a new one
                    Thread.sleep(3000 - millis % 3000);
                } else {
                    System.out.println("----Format is wrong----");
                }

            }

        } catch (Exception e) {
            ErrorSender.sendError(e.getMessage(),"error");
            e.printStackTrace();
        }
    }


}
