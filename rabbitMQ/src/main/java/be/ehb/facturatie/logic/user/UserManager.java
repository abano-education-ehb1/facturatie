package be.ehb.facturatie.logic.user;


import be.ehb.facturatie.ConnectionModel;
import be.ehb.facturatie.entity.user.User;
import be.ehb.facturatie.logic.error.ErrorSender;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;

public class UserManager {
    String user = "user";
    public static ArrayList<Integer> cache = new ArrayList<Integer>();


    public void post(User creater, String UUID) throws IOException {

        URL url = null;
        try {
            if (creater.getCompany() == null) {

                url = new URL("http://10.3.56.3:9100/api/admin/client/create?" +
                        "email=" + URLEncoder.encode(creater.getEmail(), StandardCharsets.UTF_8.toString()) +
                        "&first_name=" + URLEncoder.encode(creater.getFirst_name(), StandardCharsets.UTF_8.toString()) +
                        "&last_name=" + URLEncoder.encode(creater.getLast_name(), StandardCharsets.UTF_8.toString()) +
                        "&phone=" + URLEncoder.encode(creater.getPhone(), StandardCharsets.UTF_8.toString()) +
                        "&address_1=" + URLEncoder.encode(creater.getAddress().getStreet() + " " + creater.getAddress().getHousenumber(), StandardCharsets.UTF_8.toString()) +
                        "&country=" + URLEncoder.encode(creater.getAddress().getCountry(), StandardCharsets.UTF_8.toString()) +
                        "&city=" + URLEncoder.encode(creater.getAddress().getCity(), StandardCharsets.UTF_8.toString()) +
                        "&country=" + URLEncoder.encode(creater.getAddress().getCountry(), StandardCharsets.UTF_8.toString()) +
                        "&postcode=" + creater.getAddress().getPostalCode() +
                        "&company=none" +
                        "&company_vat=none" +
                        "&status=active" +
                        "&client_group_id=1" +
                        "&type=individual");


            } else {
                url = new URL("http://10.3.56.3:9100/api/admin/client/create?" +
                        "email=" + URLEncoder.encode(creater.getEmail(), StandardCharsets.UTF_8.toString()) +
                        "&first_name=" + URLEncoder.encode(creater.getFirst_name(), StandardCharsets.UTF_8.toString()) +
                        "&last_name=" + URLEncoder.encode(creater.getLast_name(), StandardCharsets.UTF_8.toString()) +
                        "&phone=" + URLEncoder.encode(creater.getPhone(), StandardCharsets.UTF_8.toString()) +
                        "&address_1=" + URLEncoder.encode(creater.getAddress().getStreet() + " " + creater.getAddress().getHousenumber(), StandardCharsets.UTF_8.toString()) +
                        "&country=" + URLEncoder.encode(creater.getAddress().getCountry(), StandardCharsets.UTF_8.toString()) +
                        "&city=" + URLEncoder.encode(creater.getAddress().getCity(), StandardCharsets.UTF_8.toString()) +
                        "&country=" + URLEncoder.encode(creater.getAddress().getCountry(), StandardCharsets.UTF_8.toString()) +
                        "&postcode=" + creater.getAddress().getPostalCode() +
                        "&company=" + URLEncoder.encode(creater.getCompany().getCompany_name(), StandardCharsets.UTF_8.toString()) +
                        "&company_vat=" + URLEncoder.encode(creater.getCompany().getCompany_vat(), StandardCharsets.UTF_8.toString()) +
                        "&status=active" +
                        "&client_group_id=1" +
                        "&type=company");
            }


        } catch (MalformedURLException e) {
            ErrorSender.sendError(e.getMessage(), "error");
            System.out.println("False URL" + e.getMessage());
        }
        HttpURLConnection conn = ConnectionModel.conn(url);
        try (BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"))) {
            JSONObject jsObject = ConnectionModel.jsobject(br);
            if (jsObject.get("result").toString().equals("null")) {
                ErrorSender.sendError(jsObject.get("error").toString(), "error");
                System.out.println("error");
            } else {
                //get new user_id
                int id = Integer.parseInt(jsObject.get("result").toString());
                cache.add(id);
                //Send to UUID the id of the user + the UUID
                if (creater.getCompany() == null) {
                    ConnectionModel.sendToUUID(UUID, id, "created", user);
                } else {
                    ConnectionModel.sendToUUID(UUID, id, "created", "company");
                }

            }
        } catch (Exception e) {
            ErrorSender.sendError(e.getMessage(), "error");
            e.printStackTrace();
        }


    }

    public void delete(User creater,String UUID, int id) {

        ConnectionModel.connection("delete?id=" + id);
        cache.add(id);
        if (creater.getCompany() == null) {
            ConnectionModel.sendToUUID(UUID, id, "deleted", user);
        } else {
            ConnectionModel.sendToUUID(UUID, id, "deleted", "company");
        }

    }

    public void put(User creater, String UUID, int id) {
        System.out.println("Sending update to 10.3.56.3:9100");
        // Getting the url
        URL url = null;
        try {
            if (creater.getCompany() == null) {

                url = new URL("http://10.3.56.3:9100/api/admin/client//update?id=" + id +
                        "email=" + URLEncoder.encode(creater.getEmail(), StandardCharsets.UTF_8.toString()) +
                        "&first_name=" + URLEncoder.encode(creater.getFirst_name(), StandardCharsets.UTF_8.toString()) +
                        "&last_name=" + URLEncoder.encode(creater.getLast_name(), StandardCharsets.UTF_8.toString()) +
                        "&phone=" + URLEncoder.encode(creater.getPhone(), StandardCharsets.UTF_8.toString()) +
                        "&address_1=" + URLEncoder.encode(creater.getAddress().getStreet() + " " + creater.getAddress().getHousenumber(), StandardCharsets.UTF_8.toString()) +
                        "&country=" + URLEncoder.encode(creater.getAddress().getCountry(), StandardCharsets.UTF_8.toString()) +
                        "&city=" + URLEncoder.encode(creater.getAddress().getCity(), StandardCharsets.UTF_8.toString()) +
                        "&country=" + URLEncoder.encode(creater.getAddress().getCountry(), StandardCharsets.UTF_8.toString()) +
                        "&postcode=" + creater.getAddress().getPostalCode() +
                        "&company=none" +
                        "&company_vat=none" +
                        "&status=active" +
                        "&client_group_id=1" +
                        "&type=individual");

            } else {
                url = new URL("http://10.3.56.3:9100/api/admin/client/update?id=" + id +
                        "email=" + URLEncoder.encode(creater.getEmail(), StandardCharsets.UTF_8.toString()) +
                        "&first_name=" + URLEncoder.encode(creater.getFirst_name(), StandardCharsets.UTF_8.toString()) +
                        "&last_name=" + URLEncoder.encode(creater.getLast_name(), StandardCharsets.UTF_8.toString()) +
                        "&phone=" + URLEncoder.encode(creater.getPhone(), StandardCharsets.UTF_8.toString()) +
                        "&address_1=" + URLEncoder.encode(creater.getAddress().getStreet() + " " + creater.getAddress().getHousenumber(), StandardCharsets.UTF_8.toString()) +
                        "&country=" + URLEncoder.encode(creater.getAddress().getCountry(), StandardCharsets.UTF_8.toString()) +
                        "&city=" + URLEncoder.encode(creater.getAddress().getCity(), StandardCharsets.UTF_8.toString()) +
                        "&country=" + URLEncoder.encode(creater.getAddress().getCountry(), StandardCharsets.UTF_8.toString()) +
                        "&postcode=" + creater.getAddress().getPostalCode() +
                        "&company=" + URLEncoder.encode(creater.getCompany().getCompany_name(), StandardCharsets.UTF_8.toString()) +
                        "&company_vat=" + URLEncoder.encode(creater.getCompany().getCompany_vat(), StandardCharsets.UTF_8.toString()) +
                        "&status=active" +
                        "&client_group_id=1" +
                        "&type=company");
            }
            //url = new URL("http://10.3.56.3:9100/api/admin/client/update?id=" + id + "&first_name=" + creater.getFirst_name());
        } catch (MalformedURLException | UnsupportedEncodingException e) {
            ErrorSender.sendError(e.getMessage(), "error");
            e.printStackTrace();
        }
        ConnectionModel.conn(url);
        cache.add(id);
        if (creater.getCompany() == null) {
            ConnectionModel.sendToUUID(UUID, id, "updated", user);
        } else {
            ConnectionModel.sendToUUID(UUID, id, "updated", "company");
        }
        //Send to UUID the id of the user + the UUID

    }
}
