package be.ehb.facturatie.logic.error;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;


import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeoutException;

public class ErrorSender {
    private final static String QUEUE_NAME = "errors";


  public static  void sendError(String error_message,String level){

      ConnectionFactory factory = new ConnectionFactory();
      factory.setHost("10.3.56.3");
      factory.setPort(5672);
      factory.setUsername("integration_project22");
      factory.setPassword("3jPqsaCUBXgHHLzM");
      try (Connection connection = factory.newConnection();
           Channel channel = connection.createChannel()) {
          channel.queueDeclare(QUEUE_NAME, false, false, false, null);


          String message =  "<error>\n" +
                  "    <source>facturatie</source>\n" +
                  "    <date>"+new Date().getTime() +"</date>\n" +
                  "    <level>"+level+"</level>\n" +
                  "    <message>"+error_message+"</message>\n" +
                  "</error>";


              channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
              System.out.println(" [x] Sent '" + message + "'");


      } catch (IOException e) {
          e.printStackTrace();
      } catch (TimeoutException e) {
          e.printStackTrace();
      }
  }



}
