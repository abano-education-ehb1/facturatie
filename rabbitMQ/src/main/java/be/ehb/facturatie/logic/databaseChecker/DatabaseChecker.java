package be.ehb.facturatie.logic.databaseChecker;



import be.ehb.facturatie.ConnectionModel;
import be.ehb.facturatie.entity.user.User;
import be.ehb.facturatie.logic.user.UserManager;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class DatabaseChecker {


    public static void check() {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                ArrayList<message> list;
                DatabaseConnector connector = new DatabaseConnector();

                list = connector.getData();
                if (list.size() == 0) {

                } else {
                    for (int i = 0; i < list.size(); i++) {
                        if (UserManager.cache.size() != 0) {
                            if (!UserManager.cache.contains(list.get(i).getRel_id())) {
                                if (list.get(i).get_action().equals("create")) {
                                    if (list.get(i).get_entity().equals("user")) {
                                        User user = connector.getDataForObject(list.get(i).getRel_id());
                                        ConnectionModel.sendToUUIDCreate(list.get(i).getRel_id(), list.get(i).get_action(), user, user.getAddress());
                                    } else if (list.get(i).get_entity().equals("invoice")) {

                                    }


                                } else if (list.get(i).get_action().equals("delete")) {
                                    if (list.get(i).get_entity().equals("user")) {

                                        ConnectionModel.sendToUUIDdelete(list.get(i).getRel_id(), list.get(i).get_action());
                                    } else if (list.get(i).get_entity().equals("invoice")) {

                                    }

                                } else if (list.get(i).get_action().equals("update")) {
                                    if (list.get(i).get_entity().equals("user")) {
                                        User user = connector.getDataForObject(list.get(i).getRel_id());
                                        ConnectionModel.sendToUUIDCreate(list.get(i).getRel_id(), list.get(i).get_action(), user, user.getAddress());
                                    } else if (list.get(i).get_entity().equals("invoice")) {

                                    }
                                }
                                if(UserManager.cache.indexOf(list.get(i).getRel_id()) != -1){
                                    UserManager.cache.remove(UserManager.cache.indexOf(list.get(i).getRel_id()));
                                }



                                //sendToUUID(list.get(i).getRel_id(),list.get(i).get_action(),list.get(i).get_entity());

                            }
                        } else {
                            System.out.println(list.get(i).get_action());
                            if (list.get(i).get_action().equals("create")) {

                                if (list.get(i).get_entity().equals("user")) {
                                    User user = connector.getDataForObject(list.get(i).getRel_id());
                                    ConnectionModel.sendToUUIDCreate(list.get(i).getRel_id(), list.get(i).get_action(), user, user.getAddress());
                                } else if (list.get(i).get_entity().equals("invoice")) {

                                }

                            } else if (list.get(i).get_action().equals("delete")) {
                                if (list.get(i).get_entity().equals("user")) {

                                    ConnectionModel.sendToUUIDdelete(list.get(i).getRel_id(), list.get(i).get_action());
                                } else if (list.get(i).get_entity().equals("invoice")) {

                                }

                            } else if (list.get(i).get_action().equals("update") ) {
                                if (list.get(i).get_entity().equals("user") ) {
                                    User user = connector.getDataForObject(list.get(i).getRel_id());
                                    ConnectionModel.sendToUUIDCreate(list.get(i).getRel_id(), list.get(i).get_action(), user, user.getAddress());
                                } else if (list.get(i).get_entity().equals("invoice")) {

                                }
                            }

                        }


                        connector.deletData();


                    }
                }


            }


        }, 0, 10000);
    }
}
