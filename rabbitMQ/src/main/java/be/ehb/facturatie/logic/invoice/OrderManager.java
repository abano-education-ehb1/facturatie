package be.ehb.facturatie.logic.invoice;


import be.ehb.facturatie.ConnectionModel;
import be.ehb.facturatie.entity.invoice.Order;
import be.ehb.facturatie.logic.error.ErrorSender;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class OrderManager {
    String orderke = "order";
    public void addLine(int invoiceId, Order order) {
        ConnectionModel cw = new ConnectionModel();
        double price = order.getQuantity() * order.getPrice();
        String titel = order.getQuantity() + "X " + order.getName();
        URL url = null;
        try {
            url = new URL("http://10.3.56.3:9100/api/admin/invoice/update?id=" + URLEncoder.encode(String.valueOf(invoiceId), StandardCharsets.UTF_8.toString()) + "&new_item[title]=" + URLEncoder.encode(titel,StandardCharsets.UTF_8.toString()) +"&new_item[price]="+ URLEncoder.encode(String.valueOf(price),StandardCharsets.UTF_8.toString()));
        } catch (MalformedURLException e) {
            ErrorSender.sendError(e.getMessage(),"error");
            System.out.println("False URL" + e.getMessage());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        cw.conn(url);
    }

    public static int getClientId(int orderId) {

        try {
            String result = ConnectionModel.get(new URL("http://10.3.56.3:9100/api/admin/invoice/get_list?id="+ orderId));

            return Integer.parseInt((String) ConnectionModel.getValue(new JSONObject(result), "result.list.0.client.id"));
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }
    public void updateStatus(int invoice_id, Order order) {
        ConnectionModel cw = new ConnectionModel();
        String status;
        if(order.getStatus()) {
            status = "paid";
        } else {
            status = "unpaid";
        }
        URL url = null;
        try {
            url = new URL("http://10.3.56.3:9100/api/admin/invoice/" +
                    "update?id=" + invoice_id +
                    "&status=" + status
            );

        } catch (Exception e) {
            ErrorSender.sendError(e.getMessage(),"error");
            System.out.println("False URL" + e.getMessage());
        }

        cw.conn(url);
    }

    public JSONArray getInvoiceList(int clientId) {
        URL url = null;
        try {
            url = new URL("http://10.3.56.3:9100/api/admin/invoice/get_list?client="+ clientId);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return (JSONArray) ConnectionModel.getValue(new JSONObject(ConnectionModel.get(url)),"result.list");
    }

}


