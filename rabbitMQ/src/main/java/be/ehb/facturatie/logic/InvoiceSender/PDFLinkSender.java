package be.ehb.facturatie.logic.InvoiceSender;

import be.ehb.facturatie.XML.XMLValidator;
import be.ehb.facturatie.XML.XSDEnum;
import be.ehb.facturatie.logic.error.ErrorSender;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;


import java.util.ArrayList;

public class PDFLinkSender {
    private final static String QUEUE_NAME = "mailing";

    public static void sender(ArrayList<String> emails, ArrayList<String> links) {
        String endMessage = "";
        String email = "";
        if (links.isEmpty()) {
            System.out.println("--No invoices--");
            ErrorSender.sendError("No invoices", "error");
        } else {
            for (String link : links
            ) {
                endMessage += "<link>" + link + "</link>\n";
            }

            if (emails.isEmpty()) {
                System.out.println("--no Emails--");
            } else {
                for (String email1 : emails
                ) {
                    email += "<email>" + email1 + "</email>\n";
                }
            }
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("10.3.56.3");
            factory.setPort(5672);
            factory.setUsername("integration_project22");
            factory.setPassword("3jPqsaCUBXgHHLzM");
            try (Connection connection = factory.newConnection();
                 Channel channel = connection.createChannel()) {
                channel.queueDeclare(QUEUE_NAME, false, false, false, null);

                String message =
                        "<send-invoices> \n" +
                                "<source>facturatie</source>\n" +
                                "<links>\n" +
                                endMessage +
                                "</links>\n" +
                                "<emails>\n" + email + "</emails>\n" +
                                "</send-invoices>";
                System.out.println(message);
                if (XMLValidator.validate(XSDEnum.INVOICE_SENDER, message)) {
                    channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
                    System.out.println(" [x] Sent '" + message + "'");
                } else {
                    System.out.println("Not good format invoice sender");
                }


            } catch (Exception e) {
                ErrorSender.sendError(e.getMessage(), "error");
                e.printStackTrace();
            }


        }

    }
}
