package be.ehb.facturatie.logic.invoice;


import be.ehb.facturatie.ConnectionModel;
import be.ehb.facturatie.entity.invoice.Invoice;
import be.ehb.facturatie.entity.invoice.Order;
import be.ehb.facturatie.entity.user.User;
import be.ehb.facturatie.logic.error.ErrorSender;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class InvoiceManager {
    //Sellercompany data
    String seller_company = "Erasmus Hogeschool Brussel";
    String seller_vat = "BE01 2345 6789";
    String seller_phone = "0463892809";
    String seller_adres = "Nijverheidskaai 170 1070 Anderlecht";
    String seller_email = "ehb@ehb.be";
    Invoice invoice = new Invoice();
    public void post(int clientid) {
        ConnectionModel cm = new ConnectionModel();
        URL url = null;
        try {
            url = new URL("http://10.3.56.3:9100/api/admin/invoice/prepare?client_id=" + clientid);

        } catch (MalformedURLException e) {
            ErrorSender.sendError(e.getMessage(),"error");
            System.out.println("False URL" + e.getMessage());
        }
        HttpURLConnection conn = cm.conn(url);
        try (BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"))) {
            JSONObject jsObject = cm.jsobject(br);
            if (jsObject.get("result").toString().equals("null")) {
                System.out.println("error");
            } else {
                //get new invoice_id
                int id = Integer.parseInt(jsObject.get("result").toString());
                invoice.setId(id);
                //cache.add(id);
            }
        } catch (Exception e) {
            ErrorSender.sendError(e.getMessage(),"error");
            e.printStackTrace();
        }

    }
    public void update(int id, User buyer, Order order)
    {
        String status;
        if(order.getStatus()) {
            status = "paid";
        } else {
            status = "unpaid";
        }
        ConnectionModel cw = new ConnectionModel();

        URL url = null;
        try {
            url = new URL("http://10.3.56.3:9100/api/admin/invoice/" +
                    "update?id=" + id +
                    "&buyer_first_name=" + buyer.getFirst_name() +
                    "&buyer_last_name=" + buyer.getLast_name() +
                    "&status=" + status +
                    "&seller_company=" + URLEncoder.encode(seller_company, StandardCharsets.UTF_8.toString())  +
                    "&seller_company_vat=" + URLEncoder.encode(seller_vat, StandardCharsets.UTF_8.toString())  +
                    "&seller_address=" + URLEncoder.encode(seller_adres, StandardCharsets.UTF_8.toString()) +
                    "&seller_phone=" +URLEncoder.encode(seller_phone, StandardCharsets.UTF_8.toString())  +
                    "&seller_email=" + URLEncoder.encode(seller_email, StandardCharsets.UTF_8.toString())
                    );


        } catch (Exception e) {
            ErrorSender.sendError(e.getMessage(),"error");
            System.out.println("False URL" + e.getMessage());
        }

        cw.conn(url);
    }



}
