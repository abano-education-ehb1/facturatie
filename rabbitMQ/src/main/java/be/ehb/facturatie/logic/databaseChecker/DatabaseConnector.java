package be.ehb.facturatie.logic.databaseChecker;




import be.ehb.facturatie.entity.user.Address;
import be.ehb.facturatie.entity.user.User;
import be.ehb.facturatie.logic.error.ErrorSender;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

public class DatabaseConnector {
    ArrayList<message> toDo;


    public ArrayList<message> getData() {
        try {
            toDo = new ArrayList<>();

            Class.forName("com.mysql.cj.jdbc.Driver");

            Connection con = DriverManager.getConnection("jdbc:mysql://10.3.56.3:3306/facturatie", "facturatie", "rLcp5K4WLEpy7W4hbfmz34c9xHL7DUjH");
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from message");
            while (rs.next()) {

                toDo.add(new message(rs.getString(1), rs.getString(2), rs.getInt(3)));
            }


            con.close();

        } catch (Exception e) {
            ErrorSender.sendError(e.getMessage(),"error");
            System.out.println(e);
        }
        return toDo;
    }

    public static ArrayList<String> getEmailsInvoices(int id) {
        ArrayList<String> links = new ArrayList<String>();
        try {

            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://10.3.56.3:3306/facturatie", "facturatie", "rLcp5K4WLEpy7W4hbfmz34c9xHL7DUjH");
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select hash,status from invoice where client_id =" + id);
            while (rs.next()) {
                if (rs.getString("status").equals("unpaid") ) {
                    links.add("http://10.3.56.3:9100/invoice/pdf/" + rs.getString("hash"));
                }

            }

            con.close();

        } catch (Exception e) {
            ErrorSender.sendError(e.getMessage(),"error");
            System.out.println(e);
        }
        return links;

    }
    public  static  int  getInvoiceId(int user_id){
        int id=0;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://10.3.56.3:3306/facturatie", "facturatie", "rLcp5K4WLEpy7W4hbfmz34c9xHL7DUjH");
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select id,status from invoice where client_id =" + user_id);
            while (rs.next()) {
                    id = rs.getInt("id");
            }

            con.close();

        } catch (Exception e) {
            ErrorSender.sendError(e.getMessage(),"error");
            System.out.println(e);
        }

        return id;
    }

    public static ArrayList<String> getInvoicesLinks(int id) {
        ArrayList<String> emails  = new ArrayList<String>();
        try {

            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://10.3.56.3:3306/facturatie", "facturatie", "rLcp5K4WLEpy7W4hbfmz34c9xHL7DUjH");
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select buyer_email,status from invoice where client_id =" + id);
            while (rs.next()) {
                if (rs.getString("status").equals("unpaid") ) {
                    emails.add( rs.getString("buyer_email"));
                }

            }

            con.close();

        } catch (Exception e) {
            ErrorSender.sendError(e.getMessage(),"error");
            System.out.println(e);
        }

        return emails;

    }

    public static User getDataForObject(int id) {
        User user = new User();
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            Connection con = DriverManager.getConnection("jdbc:mysql://10.3.56.3:3306/facturatie", "facturatie", "rLcp5K4WLEpy7W4hbfmz34c9xHL7DUjH");
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from client where id =" + id);
            while (rs.next()) {

                user.setFirst_name(rs.getString("first_name"));
                user.setLast_name(rs.getString("last_name"));
                user.setEmail(rs.getString("email"));
                user.setPhone(rs.getString("phone"));

                String[] split = rs.getString("address_1").split(" ");
                String street = split.length > 1 ? Arrays.stream(split).limit(split.length - 1).collect(Collectors.joining(" ")) : Arrays.stream(split).collect(Collectors.joining(" "));
                String houseNumber = split.length > 0 ? split[split.length - 1] : "";
                user.setAddress(new Address(street, houseNumber, rs.getString("city"), rs.getString("country"), rs.getString("postcode")));


            }
            con.close();
        } catch (Exception e) {
            ErrorSender.sendError(e.getMessage(),"error");
            System.out.println(e);
        }

        return user;
    }


    public void deletData() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            Connection con = DriverManager.getConnection("jdbc:mysql://10.3.56.3:3306/facturatie", "facturatie", "rLcp5K4WLEpy7W4hbfmz34c9xHL7DUjH");
            Statement stmt = con.createStatement();
            stmt.executeUpdate("delete  from message");

            ResultSet rs = stmt.executeQuery("SELECT action, entity, rel_id FROM message");
            while (rs.next()) {

                toDo.remove(new message(rs.getString(1), rs.getString(2), rs.getInt(3)));
            }
            System.out.println("----Data has been deleted----");
            con.close();
        } catch (Exception e) {
            ErrorSender.sendError(e.getMessage(),"error");
            System.out.println(e);
        }

    }

}
