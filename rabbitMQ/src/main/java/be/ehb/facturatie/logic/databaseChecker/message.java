package be.ehb.facturatie.logic.databaseChecker;

public class message {
    String action;
    String entity;
    int rel_id;

    public String get_action() {
        return action;
    }

    public String get_entity() {
        return entity;
    }

    public int getRel_id() {
        return rel_id;
    }

    public message(String action, String entity, int rel_id) {
        this.action = action;
        this.entity = entity;
        this.rel_id = rel_id;
    }
}
