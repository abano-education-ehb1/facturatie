package be.ehb.facturatie;


import be.ehb.facturatie.XML.XMLValueCatcher;
import be.ehb.facturatie.entity.invoice.Order;
import be.ehb.facturatie.entity.user.Address;
import be.ehb.facturatie.entity.user.Company;
import be.ehb.facturatie.entity.user.User;
import be.ehb.facturatie.logic.databaseChecker.DatabaseConnector;
import be.ehb.facturatie.logic.error.ErrorSender;
import be.ehb.facturatie.logic.invoice.InvoiceManager;
import be.ehb.facturatie.logic.invoice.OrderManager;
import be.ehb.facturatie.logic.user.UserManager;
import org.json.JSONArray;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.IOException;

public class Listener {
    public User userValuesSetter(XMLValueCatcher catcher, Document doc, User user) {
        user.setFirst_name(catcher.catchValue("firstname", doc));
        user.setEmail(catcher.catchValue("email", doc));
        user.setLast_name(catcher.catchValue("lastname", doc));
        user.setPhone(catcher.catchValue("phone", doc));
        return user;
    }

    public Address addressValuesSetter(XMLValueCatcher catcher, Document doc, Address address) {
        address.setStreet(catcher.catchValue("street", doc));
        address.setHousenumber(catcher.catchValue("housenumber", doc));
        address.setCountry(catcher.catchValue("country", doc));
        address.setCity(catcher.catchValue("city", doc));
        address.setPostalCode(catcher.catchValue("postalcode", doc));
        return address;
    }

    public void user(XMLValueCatcher catcher, Document doc) {
        UserManager um = new UserManager();
        //Looking if the xml file hase the action of create

        Address address = new Address();
        if (catcher.catchValue("action", doc).equals("create")) {
            User user = new User();
            if (doc.getDocumentElement().getTagName().equals("user")) {
                //get the values that we needed for the user
                user = userValuesSetter(catcher, doc, user);
                user.setAddress(addressValuesSetter(catcher, doc, address));

            } else {
                Company company = new Company();
                user.setFirst_name(catcher.catchValue("name", doc));
                user.setEmail(catcher.catchValue("email", doc));
                user.setLast_name(" ");
                user.setPhone(catcher.catchValue("phone", doc));
                company.setCompany_name(catcher.catchValue("name", doc));
                company.setCompany_vat(catcher.catchValue("taxId", doc));

                user.setAddress(addressValuesSetter(catcher, doc, address));
                user.setCompany(company);
            }

            //create the user with the values that we fetched
            try {

                um.post(user, catcher.catchValue("uuid", doc));
            } catch (IOException e) {
                ErrorSender.sendError(e.getMessage(), "error");
                e.printStackTrace();
            }
        } else if (catcher.catchValue("action", doc).equals("update")) {
            User user = new User();
            if (doc.getDocumentElement().getTagName().equals("user")) {
                //get the values that we needed for the user
                user = userValuesSetter(catcher, doc, user);
                user.setAddress(addressValuesSetter(catcher, doc, address));

            } else {
                Company company = new Company();
                user.setFirst_name(catcher.catchValue("name", doc));
                user.setEmail(catcher.catchValue("email", doc));
                user.setLast_name(" ");
                user.setPhone(catcher.catchValue("phone", doc));
                company.setCompany_name(catcher.catchValue("name", doc));
                company.setCompany_vat(catcher.catchValue("taxId", doc));

                user.setAddress(addressValuesSetter(catcher, doc, address));
                user.setCompany(company);
            }

            um.put(user, catcher.catchValue("uuid", doc), Integer.parseInt(catcher.catchValue("source-id", doc)));

        } else if (catcher.catchValue("action", doc).equals("delete")) {
            User user = new User();
            if (doc.getDocumentElement().getTagName().equals("user")) {
                //get the values that we needed for the user
                user = userValuesSetter(catcher, doc, user);
                user.setAddress(addressValuesSetter(catcher, doc, address));

            } else {
                Company company = new Company();
                user.setFirst_name(catcher.catchValue("name", doc));
                user.setEmail(catcher.catchValue("email", doc));
                user.setLast_name(" ");
                user.setPhone(catcher.catchValue("phone", doc));
                company.setCompany_name(catcher.catchValue("name", doc));
                company.setCompany_vat(catcher.catchValue("taxId", doc));

                user.setAddress(addressValuesSetter(catcher, doc, address));
                user.setCompany(company);
            }
            um.delete(user, catcher.catchValue("uuid", doc), Integer.parseInt(catcher.catchValue("source-id", doc)));

        }
    }

    public void invoice(int clientid, User user, Order order) {
        InvoiceManager im = new InvoiceManager();
        im.post(clientid);
        System.out.println("post werkt");
        int id = DatabaseConnector.getInvoiceId(clientid);


        im.update(id, user, order);


    }

    public void order(XMLValueCatcher catcher, Document doc) {
        if (catcher.catchValue("action", doc).equals("create")) {

            ConnectionModel cm = new ConnectionModel();
            OrderManager om = new OrderManager();
            Order order = new Order();


            int clientid = Integer.parseInt(XMLValueCatcher.getValue(doc, "order.properties.user.source-id"));

            JSONArray invoices = om.getInvoiceList(clientid);
            //Look at orderStatus
            order.setStatus(Boolean.valueOf(XMLValueCatcher.getValue(doc, "order.properties.is-paid")));
            System.out.println("Orderstatus: " + order.getStatus());

            if (invoices.length() > 0) {

                int orderId = Integer.parseInt((String) ConnectionModel.getValue(invoices, "0.id"));
                NodeList list = doc.getChildNodes().item(0).getChildNodes().item(4).getChildNodes().item(4).getChildNodes();

                om.updateStatus(DatabaseConnector.getInvoiceId(clientid), order);

                for (int i = 0; i < list.getLength(); i++) {
                    Node item = list.item(i);

                    if (item.getNodeName().equals("line")) {
                        String productName = XMLValueCatcher.getValue(item, "product-name");
                        double unityPrice = Double.parseDouble(XMLValueCatcher.getValue(item, "unity-price"));
                        int quantity = Integer.parseInt(XMLValueCatcher.getValue(item, "quantity"));
                        order.setPrice(unityPrice);
                        order.setName(productName);
                        order.setQuantity(quantity);
                        om.addLine(orderId, order);
                    }
                }

                cm.sendToUUID(catcher.catchValue("uuid", doc), Integer.parseInt(catcher.catchValue("source-id", doc)), "updated", "order");
            } else {


                invoice(clientid, DatabaseConnector.getDataForObject(clientid), order);
                int orderId = DatabaseConnector.getInvoiceId(clientid);

                NodeList list = XMLValueCatcher.getNode(doc, "order.properties.lines").getChildNodes();

                for (int i = 0; i < list.getLength(); i++) {
                    Node item = list.item(i);
                    if (item.getNodeName().equals("line")) {
                        String productName = XMLValueCatcher.getValue(item, "product-name");
                        double unityPrice = Double.parseDouble(XMLValueCatcher.getValue(item, "unity-price"));
                        int quantity = Integer.parseInt(XMLValueCatcher.getValue(item, "quantity"));

                        order.setPrice(unityPrice);
                        order.setName(productName);
                        order.setQuantity(quantity);
                        om.addLine(orderId, order);
                    }
                }

                 cm.sendToUUID(catcher.catchValue("uuid", doc), Integer.parseInt(catcher.catchValue("source-id", doc)), "created", "order");

            }
        }
    }
}

