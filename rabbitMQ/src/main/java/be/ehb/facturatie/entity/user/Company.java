package be.ehb.facturatie.entity.user;

public class Company {
    String company_name;
    String company_vat;
    Address company_address;
    String phone;
    String email;

    public Company() {
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getCompany_vat() {
        return company_vat;
    }

    public void setCompany_vat(String company_vat) {
        this.company_vat = company_vat;
    }

    public Address getCompany_address() {
        return company_address;
    }

    public void setCompany_address(Address company_address) {
        this.company_address = company_address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
