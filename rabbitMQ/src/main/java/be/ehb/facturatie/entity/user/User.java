package be.ehb.facturatie.entity.user;

public class User {
    private Address address;
    private Company company;

    public User(Address address, Company company, String email, String first_name, String phone, String last_name) {
        this.address = address;
        this.company = company;
        this.email = email;
        this.first_name = first_name;
        this.phone = phone;
        this.last_name = last_name;
    }

    public User() {
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    private String email;
    private String first_name;
    private String phone;
    private String last_name;


}
