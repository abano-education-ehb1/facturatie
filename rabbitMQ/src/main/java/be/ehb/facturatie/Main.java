package be.ehb.facturatie;

import be.ehb.facturatie.XML.XMLValueCatcher;
import be.ehb.facturatie.logic.InvoiceSender.PDFLinkSender;
import be.ehb.facturatie.logic.databaseChecker.DatabaseConnector;
import be.ehb.facturatie.logic.error.ErrorSender;
import be.ehb.facturatie.logic.heartbeat.Heartbeat;
import be.ehb.facturatie.logic.invoice.InvoiceManager;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

import static be.ehb.facturatie.logic.databaseChecker.DatabaseChecker.check;


public class Main {
    private final static String QUEUE_NAME = "facturatie";

    public static void main(String[] argv) {
        try {


            ConnectionModel cm = new ConnectionModel();
            Listener listener = new Listener();
            ConnectionFactory factory = cm.connectionfactory();

            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();

            //Bind the que
            String queueName = channel.queueDeclare(QUEUE_NAME, false, false, false, null).getQueue();
            //channel.queueBind(queueName, "uuid_manager_exchange", "");

            //creating a thread for our heartbeat
            Thread heartbeat = new Thread("heartbeat") {
                //Make a run for our heartbeat
                public void run() {
                    //create a class heartbeat and let the heartbeat run
                    Heartbeat beat = new Heartbeat();
                    beat.heartbeat();
                }
            };
            //creating a thread for our heartbeat
            Thread databaseCheck = new Thread("databaseCheck") {
                //Make a run for our heartbeat
                public void run() {
                    //create a class heartbeat and let the heartbeat run
                    check();
                }
            };
            //starting our heartbeat and database check
            heartbeat.start();
            databaseCheck.start();

            System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

            DeliverCallback deliverCallback = (consumerTag, delivery) -> {

                XMLValueCatcher catcher = new XMLValueCatcher();

                String message = new String(delivery.getBody(), "UTF-8");
                System.out.println(" [x] Received '" + message + "'");
                //Create a new object of DocumentBuilderFactory
                DocumentBuilderFactory dbfactory = DocumentBuilderFactory.newInstance();

           /* if(!Arrays.stream(XSDEnum.values()).anyMatch(format -> XMLValidator.validate(format, message))){
                System.out.println("Het bericht volgt het formaat niet");
                return;
            }*/


                //Create an object DocumentBuilder to parse the specified XML Data
                DocumentBuilder builder = null;
                try {
                    builder = dbfactory.newDocumentBuilder();
                    //Parse the content to Document object
                    Document doc = builder.parse(new InputSource(new StringReader(message)));

                    if (catcher.catchValue("source", doc).equals("facturatie")) {
                        System.out.println(" ----Drop because it came from us----");
                        return;
                    } else if (catcher.catchValue("action", doc).equals("get-invoice")) {
                        System.out.println("----Invoice send to mailing----");
                        ArrayList<String> list1 = DatabaseConnector.getInvoicesLinks(Integer.parseInt(catcher.catchValue("source-id", doc)));
                        ArrayList<String> list2 = DatabaseConnector.getEmailsInvoices(Integer.parseInt(catcher.catchValue("source-id", doc)));
                        PDFLinkSender.sender(list1, list2);
                    }
                    //TODO: als order al bestaat, voeg toe aan bestaande factuur
                    else if (doc.getDocumentElement().getTagName().equals("order")) {
                        listener.order(catcher, doc);
                    } else if (doc.getDocumentElement().getTagName().equals("user") || doc.getDocumentElement().getTagName().equals("company")) {
                        listener.user(catcher, doc);
                    }


                    InvoiceManager im = new InvoiceManager();
                    //im.update(new Invoice("coole beschrijving"), new User(new Address("Straat", "85", "Dilbeek", "België", "1700"), new Company("coole company naam", "BE 102.232.165"), "ehb.be", "voornaam", "telefoonnummer", "achternaam"), new SellerCompany(), 54);

                } catch (ParserConfigurationException | SAXException e) {
                    ErrorSender.sendError(e.getMessage(), "error");
                    e.printStackTrace();
                }


            };
            channel.basicConsume(queueName, true, deliverCallback, consumerTag -> {
            });
        } catch (TimeoutException e) {

            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void doWork(String task) throws InterruptedException {
        for (char ch : task.toCharArray()) {
            if (ch == '.') Thread.sleep(1000);
        }
    }
}
