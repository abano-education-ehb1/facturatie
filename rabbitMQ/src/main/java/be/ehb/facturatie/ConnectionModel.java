package be.ehb.facturatie;


import be.ehb.facturatie.XML.XMLValidator;
import be.ehb.facturatie.XML.XSDEnum;
import be.ehb.facturatie.entity.user.Address;
import be.ehb.facturatie.entity.user.User;
import be.ehb.facturatie.logic.error.ErrorSender;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Base64;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

public class ConnectionModel {

    public static void connection(String path) {
        URL url = url(path);
        HttpURLConnection conn = conn(url);
    }


    private final static String QUEUE_UUID = "uuid_manager";

    public static void sendToUUID(String UUID, int id, String action, String entity) {
        ConnectionFactory factory = connectionfactory();
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(QUEUE_UUID, false, false, false, null);

            String message =
                    "<response>\n" +
                            "<source>facturatie</source>\n" +

                            "<source-id>" + id + "</source-id>\n" +
                            "<uuid>" + UUID + "</uuid>" +
                            "<entity>" + entity + "</entity>\n" +
                            "<action>" + action + "</action>\n" +

                            "</response>";
            if (XMLValidator.validate(XSDEnum.RESPONSE, message)) {
                channel.basicPublish("", QUEUE_UUID, null, message.getBytes());
                System.out.println(" [x] Sent '" + message + "'");
            } else {
                System.out.println("Format is not supported");
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void sendToUUIDCreate(int id, String action, User user, Address adress) {
        ConnectionFactory factory = connectionfactory();
        Locale locale = new Locale("", adress.getCountry());
        String country = locale.getDisplayCountry();
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(QUEUE_UUID, false, false, false, null);
            String message = "<user>\n" + "<source>facturatie</source>\n" +
                    " <source-id>" + id + "</source-id>\n" +
                    "  <uuid></uuid>\n" +
                    "<action>" + action + "</action>\n" +
                    "  <properties>\n" +
                    "    <firstname>" + user.getFirst_name() + "</firstname>\n" +
                    "    <lastname>" + user.getLast_name() + "</lastname>\n" +
                    "    <email>" + user.getEmail() + "</email>\n" +
                    "    <address>\n" +
                    "      <street>" + adress.getStreet() + "</street>\n" +
                    "      <housenumber>" + adress.getHousenumber() + "</housenumber>\n" +
                    "      <postalcode>" + adress.getPostalCode() + "</postalcode>\n" +
                    "      <city>" + adress.getCity() + "</city>\n" +
                    "      <country>" + country + "</country>\n" +
                    "    </address>\n" +
                    "    <phone>" + user.getPhone() + "</phone>\n" +
                    "    <company>\n" +
                    "      <source-id>string</source-id>\n" +
                    "      <uuid>string</uuid>\n" +
                    "    </company>\n" +
                    "  </properties>\n" +
                    "</user>\n";
            if (XMLValidator.validate(XSDEnum.USER_CREATE, message)) {

                channel.basicPublish("", QUEUE_UUID, null, message.getBytes());
                System.out.println(" [x] Sent '" + message + "'");

            } else {
                System.out.println("Format is not supported");
            }


        } catch (Exception e) {
            ErrorSender.sendError(e.getMessage(), "error");
            e.printStackTrace();
        }


    }

    public static void sendToUUIDdelete(int id, String action) {
        ConnectionFactory factory = connectionfactory();
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(QUEUE_UUID, false, false, false, null);
            String message = "<user>\n" + " <source>facturatie</source>\n" +
                    " <source-id>" + id + "</source-id>\n" +
                    "  <uuid></uuid>\n" +
                    "<action>" + action + "</action>\n" +
                    "  <properties>\n" +
                    "    <firstname>" + "</firstname>\n" +
                    "    <lastname>" + "</lastname>\n" +
                    "    <email>" + "</email>\n" +
                    "    <address>\n" +
                    "      <street>" + "</street>\n" +
                    "      <housenumber>" + 65 + "</housenumber>\n" +
                    "      <postalcode>" + 1500 + "</postalcode>\n" +
                    "      <city>" + "</city>\n" +
                    "      <country>" + "</country>\n" +
                    "    </address>\n" +
                    "    <phone>" + "</phone>\n" +
                    "    <company>\n" +
                    "      <source-id>string</source-id>\n" +
                    "      <uuid>string</uuid>\n" +
                    "    </company>\n" +
                    "  </properties>\n" +
                    "</user>\n";
            if (XMLValidator.validate(XSDEnum.USER_DELETE, message)) {
                channel.basicPublish("", QUEUE_UUID, null, message.getBytes());
                System.out.println(" [x] Sent '" + message + "'");
            } else {
                System.out.println("Format is not supported");
            }


        } catch (Exception e) {
            ErrorSender.sendError(e.getMessage(), "error");
            e.printStackTrace();
        }


    }

    public static ConnectionFactory connectionfactory() {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("10.3.56.3");
        factory.setPort(5672);
        factory.setUsername("integration_project22");
        factory.setPassword("3jPqsaCUBXgHHLzM");
        return factory;
    }


    public static URL url(String path) {
        // Getting the url
        URL url = null;
        try {
            url = new URL("http://10.3.56.3:9100/api/admin/client/" + path);
        } catch (MalformedURLException e) {
            ErrorSender.sendError(e.getMessage(), "error");
            System.out.println("Bad URL:" + e.getMessage());
        }
        return url;
    }

    public static HttpURLConnection conn(URL url) {
        //Open connection
        HttpURLConnection conn = null;
        try {
            conn = (HttpURLConnection) url.openConnection();
        } catch (IOException e) {
            ErrorSender.sendError(e.getMessage(), "error");
            System.out.println("Bad connection" + e.getMessage());
        }
        //credentials to access api
        String userCredentials = "admin:yjCesPoqunRKiAAahtSNqvlssyqkPn6v";
        //Convert it
        String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userCredentials.getBytes()));

        //set header with those credentials
        conn.setRequestProperty("Authorization", basicAuth);
        conn.setRequestProperty("Content-Type", "application/json");

        try {
            System.out.println("Response: "+ conn.getResponseMessage());

        } catch (IOException e) {
            ErrorSender.sendError(e.getMessage(), "error");
            System.out.println("No response: " + e.getMessage());
        }
        if (conn == null) {
            System.out.printf("Error: Connection is null");
        }
        return conn;
    }

    public static String get(URL url) {

        HttpURLConnection conn = conn(url);
        try (BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"))) {
            return br.lines().collect(Collectors.joining());
        } catch (Exception e) {
            ErrorSender.sendError(e.getMessage(), "error");
            e.printStackTrace();
        }
        return null;
    }

    public static Object getValue(JSONObject obj, String path) {
        final String[] split = path.split("\\.");
        final Object value = obj.get(split[0]);
        if (value == null)
            return null;
        else if (split.length > 1 && value instanceof JSONObject)
            return getValue((JSONObject) value, Arrays.stream(split).skip(1).collect(Collectors.joining(".")));
        else if (split.length > 1 && value instanceof JSONArray)
            return getValue((JSONArray) value, Arrays.stream(split).skip(1).collect(Collectors.joining(".")));
        return value;
    }

    public static Object getValue(JSONArray obj, String path) {
        final String[] split = path.split("\\.");
        final Object value = obj.get(Integer.parseInt(split[0]));
        if (value == null)
            return null;
        else if (split.length > 1 && value instanceof JSONObject)
            return getValue((JSONObject) value, Arrays.stream(split).skip(1).collect(Collectors.joining(".")));
        else if (split.length > 1 && value instanceof JSONArray)
            return getValue((JSONArray) value, Arrays.stream(split).skip(1).collect(Collectors.joining(".")));
        return value;
    }


    public static JSONObject jsobject(BufferedReader br) {
        StringBuilder response = new StringBuilder();
        String responseLine = null;
        while (true) {
            try {
                if (!((responseLine = br.readLine()) != null)) break;
            } catch (IOException e) {
                ErrorSender.sendError(e.getMessage(), "error");
                e.printStackTrace();
            }
            response.append(responseLine.trim());
        }
        System.out.println(response.toString());
        //Getting the id of our fresh created user
        JSONObject jsObject = new JSONObject(response.toString());
        return jsObject;
    }
}
