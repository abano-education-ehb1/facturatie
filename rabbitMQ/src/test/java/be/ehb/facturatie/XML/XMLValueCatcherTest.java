package be.ehb.facturatie.XML;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;

import static org.junit.jupiter.api.Assertions.*;

class XMLValueCatcherTest {
String testXml;
DocumentBuilder builder;
Document doc;
XMLValueCatcher catcher;
DocumentBuilderFactory dbfactory;

    @BeforeEach
    void setUp() {
        dbfactory = DocumentBuilderFactory.newInstance();
        testXml ="<user>\n" +
                "  <source>frontend</source>\n" +
                "  <source-id>string</source-id>\n" +
                "  <uuid>string</uuid>\n" +
                "  <action>update</action>\n" +
                "  <properties>\n" +
                "    <firstname>wannes</firstname>\n" +
                "    <lastname>string</lastname>\n" +
                "    <email>string</email>\n" +
                "    <address>\n" +
                "      <street>string</street>\n" +
                "      <housenumber>4635</housenumber>\n" +
                "      <postalcode>1802</postalcode>\n" +
                "      <city>string</city>\n" +
                "      <country>string</country>\n" +
                "    </address>\n" +
                "    <phone>string</phone>\n" +
                "    <company>\n" +
                "      <source-id></source-id>\n" +
                "      <uuid></uuid>\n" +
                "    </company>\n" +
                "  </properties>\n" +
                "</user>";

        try {
            builder = dbfactory.newDocumentBuilder();
            doc = builder.parse(new InputSource(new StringReader(testXml)));
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        catcher =new XMLValueCatcher();
    }
    @Test
    public void testWhenCatchDoesNotFindTag(){
        Assertions.assertNull(catcher.catchValue("test",doc));
    }
    @Test
    public void testWhenCatchDoesFindTag(){
        Assertions.assertEquals(catcher.catchValue("firstname",doc),"wannes");
    }
    @Test
    public void testWhenCatchHasNotSameValue(){
        Assertions.assertNotEquals(catcher.catchValue("firstname",doc),"test");
    }
}