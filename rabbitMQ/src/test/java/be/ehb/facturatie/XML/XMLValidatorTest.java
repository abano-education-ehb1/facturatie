package be.ehb.facturatie.XML;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.xml.sax.SAXParseException;

import static org.junit.jupiter.api.Assertions.*;

class XMLValidatorTest {
    private XMLValidator validator;
    private XSDEnum xsdEnum;
    private String correctUUIDxml,corrextCreatexml,wrongxtCreatexml,correctHeartbeatxml,wrongHeartbeatxml,correctErrorxml,wrongErrorxml;
    private  String wrongUUIDxml,wrongInvoiceSenderxml,correctInvoiceSenderxml,correctResponsexml,wrongResponsexml;

    @BeforeEach
    void setUp() {
        // test if it validate
        validator = new XMLValidator();
        correctUUIDxml = "<response>\n" +
                "  <source>kassa</source>\n" +
                "  <source-id>55</source-id>\n" +
                "  <uuid>2ccf2a11-9cb0-42fe-9d5f-0e4d21f8c9e5</uuid>\n" +
                "  <entity>user</entity>\n" +
                "  <action>created</action>\n" +
                "</response>";
        wrongUUIDxml ="<user>\n" +
                "  <source>kassa</source>\n" +
                "  <source-id>55</source-id>\n" +
                "  <uuid>2ccf2a11-9cb0-42fe-9d5f-0e4d21f8c9e5</uuid>\n" +
                "  <entity>user</entity>\n" +
                "  <action>created</action>\n" +
                "</user>";
        corrextCreatexml="<user>\n" +
                "  <source>frontend</source>\n" +
                "  <source-id>string</source-id>\n" +
                "  <uuid>string</uuid>\n" +
                "  <action>create</action>\n" +
                "  <properties>\n" +
                "    <firstname>Joske</firstname>\n" +
                "    <lastname>Vermeule</lastname>\n" +
                "    <email>joske@gmail.com</email>\n" +
                "    <address>\n" +
                "      <street>schoten</street>\n" +
                "      <housenumber>4635</housenumber>\n" +
                "      <postalcode>1802</postalcode>\n" +
                "      <city>schoten</city>\n" +
                "      <country>Texas</country>\n" +
                "    </address>\n" +
                "    <phone>0470504599</phone>\n" +
                "    <company>\n" +
                "      <source-id></source-id>\n" +
                "      <uuid></uuid>\n" +
                "    </company>\n" +
                "  </properties>\n" +
                "</user>";
        wrongxtCreatexml="<user>\n" +
                "  <source>frontend</source>\n" +
                "  <source-id>string</source-id>\n" +
                "  <uuid>string</uuid>\n" +
                "  <action>tester</action>\n" +
                "  <properties>\n" +
                "    <firstname>Joske</firstname>\n" +
                "    <lastname>Vermeule</lastname>\n" +
                "    <email>joske@gmail.com</email>\n" +
                "    <address>\n" +
                "      <street>schoten</street>\n" +
                "      <housenumber>4635</housenumber>\n" +
                "      <postalcode>1802</postalcode>\n" +
                "      <city>schoten</city>\n" +
                "      <country>Texas</country>\n" +
                "    </address>\n" +
                "    <phone>0470504599</phone>\n" +
                "    <company>\n" +
                "      <source-id></source-id>\n" +
                "      <uuid></uuid>\n" +
                "    </company>\n" +
                "  </properties>\n" +
                "</user>";
        correctHeartbeatxml="<heartbeat>\n" +
                "    <source>frontend</source>\n" +
                "    <date>1650968324</date>\n" +
                "</heartbeat>";
        wrongHeartbeatxml="<test>\n" +
                "    <source>frontend</source>\n" +
                "    <date>1650968324</date>\n" +
                "</test>";
        correctErrorxml="<error>\n" +
                "    <source>frontend</source>\n" +
                "    <date>1650968324</date>\n" +
                "    <level>warn</level>\n" +
                "    <message>This is an error</message>\n" +
                "</error>";
        wrongErrorxml="<error>\n" +
                "    <test>frontend</test>\n" +
                "    <date>1650968324</date>\n" +
                "    <level>warn</level>\n" +
                "    <message>This is an error</message>\n" +
                "</error>";
        wrongInvoiceSenderxml="<send>\n" +
                "   <source>facturatie</source>\n" +
                "   <links>\n" +
                "      <link>hierin staat er een link van de pdf bestand</link>\n" +
                "   </links>\n" +
                "   <emails>\n" +
                "      <email>Hierin zit een email</email>\n" +
                "   </emails>\n" +
                "</send>";
        correctInvoiceSenderxml="<send-invoices>\n" +
                        "   <source>facturatie</source>\n" +
                        "   <links>\n" +
                        "      <link>hierin staat er een link van de pdf bestand</link>\n" +
                        "   </links>\n" +
                        "   <emails>\n" +
                        "      <email>Hierin zit een email</email>\n" +
                        "   </emails>\n" +
                        "</send-invoices>";
        correctResponsexml="<response>\n" +
                "  <source>kassa</source>\n" +
                "  <source-id>55</source-id>\n" +
                "  <uuid>2ccf2a11-9cb0-42fe-9d5f-0e4d21f8c9e5</uuid>\n" +
                "  <entity>user</entity>\n" +
                "  <action>created</action>\n" +
                "</response>";
        wrongResponsexml="<response>\n" +
                "  <sour>kassa</sour>\n" +
                "  <source-id>55</source-id>\n" +
                "  <uuid>2ccf2a11-9cb0-42fe-9d5f-0e4d21f8c9e5</uuid>\n" +
                "  <entity>user</entity>\n" +
                "  <action>created/updated/deleted</action>\n" +
                "</response>";

    }


    @Test
    void validateWhenFailUUIDxsd() {
        Assertions.assertFalse(XMLValidator.validate(XSDEnum.UUID,wrongUUIDxml));
    }
    @Test
    public void validateWhenSucceedUUIDxsd(){
        Assertions.assertTrue( XMLValidator.validate(XSDEnum.UUID,correctUUIDxml));
    }
    @Test
    public void validateWhenSucceedCreatexsd(){
        Assertions.assertTrue( XMLValidator.validate(XSDEnum.USER_CREATE,corrextCreatexml));
    }
    @Test
    public void validateWhenFailedCreatexsd(){
        Assertions.assertFalse( XMLValidator.validate(XSDEnum.USER_CREATE,wrongxtCreatexml));
    }
    @Test
    public void validateWhenSucceedHeartbeatxsd(){
        Assertions.assertTrue( XMLValidator.validate(XSDEnum.HEARTBEAT,correctHeartbeatxml));
    }
    @Test
    public void validateWhenFailedHeartbeatxsd(){
        Assertions.assertFalse( XMLValidator.validate(XSDEnum.HEARTBEAT,wrongHeartbeatxml));
    }

    @Test
    public void validateWhenFailedErrorxsd(){
        Assertions.assertFalse( XMLValidator.validate(XSDEnum.ERROR,wrongErrorxml));
    }
    @Test
    public void validateWhenSucceedErrorxsd(){
        Assertions.assertTrue( XMLValidator.validate(XSDEnum.ERROR,correctErrorxml));
    }
    @Test
    public void validateWhenSucceedInvoice_senderxsd(){
        Assertions.assertTrue( XMLValidator.validate(XSDEnum.INVOICE_SENDER,correctInvoiceSenderxml));
    }
    @Test
    public void validateWhenFailedInvoice_senderxsd(){
        Assertions.assertFalse( XMLValidator.validate(XSDEnum.INVOICE_SENDER,wrongInvoiceSenderxml));
    }
    @Test
    public void validateWhenFailedResponsexsd(){
        Assertions.assertFalse( XMLValidator.validate(XSDEnum.RESPONSE,wrongResponsexml));
    }
    @Test
    public void validateWhenSucceedResponsexsd(){
        Assertions.assertTrue( XMLValidator.validate(XSDEnum.RESPONSE,correctResponsexml));
    }



}