<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__93a751759fa85c3300e1e13efc865aede2e8ec1cecfaf626036e054378b8dd2a */
class __TwigTemplate_25285f0939fab98db6348f6430b6c391c7cbcc2620572f3392d80f3d12d53f6f extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "
";
        // line 2
        ob_start();
        // line 3
        echo "
Hello ";
        // line 4
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["c"] ?? null), "first_name", [], "any", false, false, false, 4), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["c"] ?? null), "last_name", [], "any", false, false, false, 4), "html", null, true);
        echo ",

Your *";
        // line 6
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "title", [], "any", false, false, false, 6), "html", null, true);
        echo "* that was activated on *";
        echo twig_escape_filter($this->env, $this->extensions['Box_TwigExtensions']->twig_bb_date(twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "activated_at", [], "any", false, false, false, 6)), "html", null, true);
        echo "* is now canceled
";
        // line 7
        if (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "reason", [], "any", false, false, false, 7)) {
            echo " Reason:     

**";
            // line 9
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "reason", [], "any", false, false, false, 9), "html", null, true);
            echo "** ";
        }
        echo "   

If you have any questions regarding this message please login to the members area and submit a support ticket.

Login to members area: ";
        // line 13
        echo $this->extensions['Box_TwigExtensions']->twig_bb_client_link_filter("login", ["email" => twig_get_attribute($this->env, $this->source, ($context["c"] ?? null), "email", [], "any", false, false, false, 13)]);
        echo "
Submit support ticket: ";
        // line 14
        echo $this->extensions['Box_TwigExtensions']->twig_bb_client_link_filter("support", ["ticket" => 1]);
        echo "

";
        // line 16
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["guest"] ?? null), "system_company", [], "any", false, false, false, 16), "signature", [], "any", false, false, false, 16), "html", null, true);
        echo "

";
        $___internal_parse_0_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 2
        echo twig_markdown_filter($this->env, $___internal_parse_0_);
    }

    public function getTemplateName()
    {
        return "__string_template__93a751759fa85c3300e1e13efc865aede2e8ec1cecfaf626036e054378b8dd2a";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 2,  81 => 16,  76 => 14,  72 => 13,  63 => 9,  58 => 7,  52 => 6,  45 => 4,  42 => 3,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("
{% apply markdown %}

Hello {{ c.first_name }} {{ c.last_name }},

Your *{{ order.title }}* that was activated on *{{ order.activated_at|bb_date }}* is now canceled
{% if order.reason %} Reason:     

**{{ order.reason }}** {% endif %}   

If you have any questions regarding this message please login to the members area and submit a support ticket.

Login to members area: {{'login'|link({'email' : c.email }) }}
Submit support ticket: {{ 'support'|link({'ticket' : 1}) }}

{{ guest.system_company.signature }}

{% endapply %}
", "__string_template__93a751759fa85c3300e1e13efc865aede2e8ec1cecfaf626036e054378b8dd2a", "");
    }
}
