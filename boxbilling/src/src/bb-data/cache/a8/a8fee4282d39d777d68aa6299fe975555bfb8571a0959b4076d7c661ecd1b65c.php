<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mod_kb_article.phtml */
class __TwigTemplate_46cde1b86843a44dc247f1b1cac859a8985dd6d342df34358ec9c00258bba0d1 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'meta_title' => [$this, 'block_meta_title'],
            'content' => [$this, 'block_content'],
            'content_after' => [$this, 'block_content_after'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return $this->loadTemplate(((twig_get_attribute($this->env, $this->source, ($context["request"] ?? null), "ajax", [], "any", false, false, false, 1)) ? ("layout_blank.phtml") : ("layout_default.phtml")), "mod_kb_article.phtml", 1);
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_meta_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["article"] ?? null), "title", [], "any", false, false, false, 3), "html", null, true);
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
    <div class=\"h-block\">
        <div class=\"h-block-header\">
            <div class=\"icon\"><span class=\"big-light-icon i-kb\"></span></div>
            <h2>";
        // line 10
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["article"] ?? null), "title", [], "any", false, false, false, 10), "html", null, true);
        echo "</h2>
            <p class=\"meta\">";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Box_TwigExtensions']->twig_bb_date(twig_get_attribute($this->env, $this->source, ($context["article"] ?? null), "created_at", [], "any", false, false, false, 11)), "html", null, true);
        echo " | ";
        echo gettext("Views");
        echo ": ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["article"] ?? null), "views", [], "any", false, false, false, 11), "html", null, true);
        echo "</p>
        </div>
        <div class=\"block\">

            ";
        // line 25
        echo "
            <div class=\"box articles\">
                <div class=\"block article first last\">
                    ";
        // line 28
        echo twig_bbmd_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["article"] ?? null), "content", [], "any", false, false, false, 28));
        echo "
                </div>
            </div>
        </div>
    </div>

    <p><a class=\"bb-button\" href=\"";
        // line 34
        echo $this->extensions['Box_TwigExtensions']->twig_bb_client_link_filter("kb");
        echo "\"><span class=\"dark-icon i-arrow\"></span> ";
        echo gettext("Back to list");
        echo "</a></p>
";
    }

    // line 37
    public function block_content_after($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 38
        echo "    <div class=\"grid_6 alpha\">
        <div class=\"widget\">
            <div class=\"head\">
                <h2 class=\"dark-icon i-kb\">";
        // line 41
        echo gettext("Similar articles");
        echo "</h2>
            </div>
            <ul class=\"menu\">
            ";
        // line 44
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["guest"] ?? null), "kb_article_get_list", [0 => ["kb_article_category_id" => twig_get_attribute($this->env, $this->source, ($context["article"] ?? null), "kb_article_category_id", [], "any", false, false, false, 44), "per_page" => 5]], "method", false, false, false, 44), "list", [], "any", false, false, false, 44));
        foreach ($context['_seq'] as $context["i"] => $context["kbarticle"]) {
            // line 45
            echo "                <li><a href=\"";
            echo $this->extensions['Box_TwigExtensions']->twig_bb_client_link_filter("/kb");
            echo "/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["article"] ?? null), "category", [], "any", false, false, false, 45), "slug", [], "any", false, false, false, 45), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["kbarticle"], "slug", [], "any", false, false, false, 45), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_truncate_filter($this->env, twig_get_attribute($this->env, $this->source, $context["kbarticle"], "title", [], "any", false, false, false, 45), 45), "html", null, true);
            echo "</a></li>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['i'], $context['kbarticle'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 47
        echo "            </ul>
        </div>
</div>
<div class=\"grid_6 omega\">
        <div class=\"widget\">
            <div class=\"head\">
                <h2 class=\"dark-icon i-kb\">";
        // line 53
        echo gettext("Knowledge base categories");
        echo "</h2>
            </div>
            <ul class=\"menu\">
            ";
        // line 56
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["guest"] ?? null), "kb_category_get_list", [0 => ["per_page" => 5]], "method", false, false, false, 56), "list", [], "any", false, false, false, 56));
        foreach ($context['_seq'] as $context["i"] => $context["category"]) {
            // line 57
            echo "                <li><a href=\"";
            echo $this->extensions['Box_TwigExtensions']->twig_bb_client_link_filter("/kb");
            echo "#category-";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "id", [], "any", false, false, false, 57), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "title", [], "any", false, false, false, 57), "html", null, true);
            echo "</a></li>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['i'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 59
        echo "            </ul>
        </div>
</div>

";
    }

    public function getTemplateName()
    {
        return "mod_kb_article.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  166 => 59,  153 => 57,  149 => 56,  143 => 53,  135 => 47,  120 => 45,  116 => 44,  110 => 41,  105 => 38,  101 => 37,  93 => 34,  84 => 28,  79 => 25,  68 => 11,  64 => 10,  58 => 6,  54 => 5,  47 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends request.ajax ? \"layout_blank.phtml\" : \"layout_default.phtml\" %}

{% block meta_title %}{{article.title}}{% endblock %}

{% block content %}

    <div class=\"h-block\">
        <div class=\"h-block-header\">
            <div class=\"icon\"><span class=\"big-light-icon i-kb\"></span></div>
            <h2>{{article.title}}</h2>
            <p class=\"meta\">{{article.created_at|bb_date }} | {% trans 'Views' %}: {{ article.views }}</p>
        </div>
        <div class=\"block\">

            {#
            <div class=\"block\" style=\"text-align: center;\">
                <form method=\"get\" action=\"{{ 'kb'|link }}\" class=\"search\">
                    <p>
                        <input class=\"search text\" name=\"q\" type=\"text\" value=\"{{ request.q }}\" placeholder=\"{% trans 'What are you looking for?' %}\">
                        <input class=\"bb-button bb-button-submit\" value=\"{% trans 'Search'%}\" type=\"submit\">
                    </p>
                </form>
            </div>
            #}

            <div class=\"box articles\">
                <div class=\"block article first last\">
                    {{article.content|bbmd}}
                </div>
            </div>
        </div>
    </div>

    <p><a class=\"bb-button\" href=\"{{ 'kb'|link }}\"><span class=\"dark-icon i-arrow\"></span> {% trans 'Back to list' %}</a></p>
{% endblock %}

{% block content_after %}
    <div class=\"grid_6 alpha\">
        <div class=\"widget\">
            <div class=\"head\">
                <h2 class=\"dark-icon i-kb\">{%trans 'Similar articles' %}</h2>
            </div>
            <ul class=\"menu\">
            {% for i, kbarticle in guest.kb_article_get_list({\"kb_article_category_id\":article.kb_article_category_id, \"per_page\":5}).list %}
                <li><a href=\"{{ '/kb'|link }}/{{article.category.slug}}/{{kbarticle.slug}}\">{{kbarticle.title|truncate(45)}}</a></li>
            {% endfor %}
            </ul>
        </div>
</div>
<div class=\"grid_6 omega\">
        <div class=\"widget\">
            <div class=\"head\">
                <h2 class=\"dark-icon i-kb\">{%trans 'Knowledge base categories'%}</h2>
            </div>
            <ul class=\"menu\">
            {% for i, category in guest.kb_category_get_list({\"per_page\":5}).list %}
                <li><a href=\"{{ '/kb'|link }}#category-{{category.id}}\">{{category.title}}</a></li>
            {% endfor %}
            </ul>
        </div>
</div>

{% endblock %}
", "mod_kb_article.phtml", "/var/www/html/bb-themes/boxbilling/html/mod_kb_article.phtml");
    }
}
