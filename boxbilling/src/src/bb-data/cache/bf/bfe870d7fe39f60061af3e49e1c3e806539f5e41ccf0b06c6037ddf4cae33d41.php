<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mod_formbuilder_preview.phtml */
class __TwigTemplate_abd19b126d9830064f345dc79f0c736d8a8b93c702d350f632935265f27323fb extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"rowElem preview\">
    <label for=\"";
        // line 2
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "name", [], "any", false, false, false, 2), "html", null, true);
        echo "\">";
        if ((twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "hide_label", [], "any", false, false, false, 2) != 1)) {
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "label", [], "any", false, false, false, 2), "html", null, true);
        }
        echo "</label>

    <div class=\"formRight\">
        ";
        // line 5
        if ((twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "type", [], "any", false, false, false, 5) == "text")) {
            // line 6
            echo "        <div class=\"moreFields allUpper\">
            <ul>
                <li class=\"sep\">";
            // line 8
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "prefix", [], "any", false, false, false, 8), "html", null, true);
            echo "</li>
                <li style=\"width: 50%\">
                    <input type=\"text\" name=\"";
            // line 10
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "name", [], "any", false, false, false, 10), "html", null, true);
            echo "\" value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "default_value", [], "any", false, false, false, 10), "html", null, true);
            echo "\"  id=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "name", [], "any", false, false, false, 10), "html", null, true);
            echo "\"
                        ";
            // line 11
            echo (((twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "readonly", [], "any", false, false, false, 11) == 1)) ? ("readonly=\"readonly\"") : (""));
            echo "
                    />
                </li>
                <li class=\"sep\">";
            // line 14
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "suffix", [], "any", false, false, false, 14), "html", null, true);
            echo "</li>
            </ul>
        </div>
        ";
        } elseif ((twig_get_attribute($this->env, $this->source,         // line 17
($context["field"] ?? null), "type", [], "any", false, false, false, 17) == "select")) {
            // line 18
            echo "            ";
            if (twig_test_empty(twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "options", [], "any", false, false, false, 18))) {
                // line 19
                echo "            <ul>
                <li style=\"width: 80%\"><blockquote>";
                // line 20
                echo gettext("Please click on \"Edit\" in order to add new select box");
                echo "</blockquote></li>
            </ul>
            ";
            } else {
                // line 23
                echo "                <select name=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "name", [], "any", false, false, false, 23), "html", null, true);
                echo "\" required=\"required\"   id=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "name", [], "any", false, false, false, 23), "html", null, true);
                echo "\"
                    ";
                // line 24
                echo (((twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "readonly", [], "any", false, false, false, 24) == 1)) ? ("disabled=\"disabled\"") : (""));
                echo "
                    />
                    ";
                // line 26
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "options", [], "any", false, false, false, 26));
                foreach ($context['_seq'] as $context["k"] => $context["v"]) {
                    // line 27
                    echo "                    <option value=\"";
                    echo twig_escape_filter($this->env, $context["v"], "html", null, true);
                    echo "\"
                    ";
                    // line 28
                    echo (((twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "default_value", [], "any", false, false, false, 28) == $context["v"])) ? ("selected") : (""));
                    echo ">";
                    echo twig_escape_filter($this->env, $context["k"], "html", null, true);
                    echo "</option>
                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['k'], $context['v'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 30
                echo "
                </select>
            ";
            }
            // line 33
            echo "        ";
        } elseif ((twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "type", [], "any", false, false, false, 33) == "checkbox")) {
            // line 34
            echo "            ";
            if (twig_test_empty(twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "options", [], "any", false, false, false, 34))) {
                // line 35
                echo "            <ul>
                <li style=\"width: 80%\"><blockquote>";
                // line 36
                echo gettext("Please click on \"Edit\" in order to add new checkbox");
                echo "</blockquote></li>
            </ul>
            ";
            } else {
                // line 39
                echo "                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "options", [], "any", false, false, false, 39));
                foreach ($context['_seq'] as $context["k"] => $context["v"]) {
                    // line 40
                    echo "                <input type=\"checkbox\" name=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "name", [], "any", false, false, false, 40), "html", null, true);
                    echo "\" value=\"";
                    echo twig_escape_filter($this->env, $context["v"], "html", null, true);
                    echo "\" id=\"";
                    echo twig_escape_filter($this->env, $context["k"], "html", null, true);
                    echo "_";
                    echo twig_escape_filter($this->env, $context["v"], "html", null, true);
                    echo "\" ";
                    if (twig_in_filter($context["v"], twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "default_value", [], "any", false, false, false, 40))) {
                        echo "checked=\"checked\"";
                    }
                    // line 41
                    echo "                    ";
                    echo (((twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "readonly", [], "any", false, false, false, 41) == 1)) ? ("disabled=\"disabled\"") : (""));
                    echo "
            />
                <label for=\"";
                    // line 43
                    echo twig_escape_filter($this->env, $context["k"], "html", null, true);
                    echo "_";
                    echo twig_escape_filter($this->env, $context["v"], "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $context["k"], "html", null, true);
                    echo "</label>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['k'], $context['v'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 45
                echo "            ";
            }
            // line 46
            echo "        ";
        } elseif ((twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "type", [], "any", false, false, false, 46) == "radio")) {
            // line 47
            echo "            ";
            if (twig_test_empty(twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "options", [], "any", false, false, false, 47))) {
                // line 48
                echo "                <ul>
                    <li style=\"width: 80%\"><blockquote>";
                // line 49
                echo gettext("Please click on \"Edit\" in order to add new radio box");
                echo "</blockquote></li>
                </ul>
            ";
            } else {
                // line 52
                echo "                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "options", [], "any", false, false, false, 52));
                foreach ($context['_seq'] as $context["k"] => $context["v"]) {
                    // line 53
                    echo "                <input type=\"radio\" name=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "name", [], "any", false, false, false, 53), "html", null, true);
                    echo "\" value=\"";
                    echo twig_escape_filter($this->env, $context["v"], "html", null, true);
                    echo "\" id=\"";
                    echo twig_escape_filter($this->env, $context["k"], "html", null, true);
                    echo "_";
                    echo twig_escape_filter($this->env, $context["v"], "html", null, true);
                    echo "\" ";
                    echo (((twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "default_value", [], "any", false, false, false, 53) == $context["v"])) ? ("checked") : (""));
                    echo "
                    ";
                    // line 54
                    echo (((twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "readonly", [], "any", false, false, false, 54) == 1)) ? ("disabled=\"disabled\"") : (""));
                    echo "
            />
                <label for=\"";
                    // line 56
                    echo twig_escape_filter($this->env, $context["k"], "html", null, true);
                    echo "_";
                    echo twig_escape_filter($this->env, $context["v"], "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $context["k"], "html", null, true);
                    echo "</label>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['k'], $context['v'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 58
                echo "            ";
            }
            // line 59
            echo "        ";
        } elseif ((twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "type", [], "any", false, false, false, 59) == "textarea")) {
            // line 60
            echo "            <div style=\"width: auto; max-width:50%;\">
                <textarea name=\"";
            // line 61
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "name", [], "any", false, false, false, 61), "html", null, true);
            echo "\" style=\"width: ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "options", [], "any", false, false, false, 61), "width", [], "any", false, false, false, 61), "html", null, true);
            echo "px; height: ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "options", [], "any", false, false, false, 61), "height", [], "any", false, false, false, 61), "html", null, true);
            echo "px; overflow: scroll;\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "default_value", [], "any", false, false, false, 61), "html", null, true);
            echo "</textarea>
            </div>
        ";
        }
        // line 64
        echo "        </div>
        <div class=\"fix\"></div>

    <div class=\"actions\" style=\"right: 0; bottom: 0; padding: 5px; position: absolute;\">
<!--        <a class=\"btn14 pr btnIconLeft\" href=\"#\" style=\"display: none;\"><img src=\"images/icons/dark/preview.png\" alt=\"Preview\" class=\"icon\">Preview</a>-->
        <a class=\"btn14 ed\" href=\"#\"><img src=\"images/icons/dark/pencil.png\" alt=\"Edit\" class=\"icon\"></a>
        <a class=\"btn14 rm\" href=\"#\" data-field-id=\"";
        // line 70
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "id", [], "any", false, false, false, 70), "html", null, true);
        echo "\"><img src=\"images/icons/dark/trash.png\" alt=\"\" class=\"icon\"></a>
    </div>
    <div class=\"fix\"></div>

</div>";
    }

    public function getTemplateName()
    {
        return "mod_formbuilder_preview.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  264 => 70,  256 => 64,  244 => 61,  241 => 60,  238 => 59,  235 => 58,  223 => 56,  218 => 54,  205 => 53,  200 => 52,  194 => 49,  191 => 48,  188 => 47,  185 => 46,  182 => 45,  170 => 43,  164 => 41,  151 => 40,  146 => 39,  140 => 36,  137 => 35,  134 => 34,  131 => 33,  126 => 30,  116 => 28,  111 => 27,  107 => 26,  102 => 24,  95 => 23,  89 => 20,  86 => 19,  83 => 18,  81 => 17,  75 => 14,  69 => 11,  61 => 10,  56 => 8,  52 => 6,  50 => 5,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"rowElem preview\">
    <label for=\"{{field.name}}\">{% if field.hide_label != 1 %}{{ field.label }}{% endif %}</label>

    <div class=\"formRight\">
        {% if field.type == \"text\" %}
        <div class=\"moreFields allUpper\">
            <ul>
                <li class=\"sep\">{{field.prefix}}</li>
                <li style=\"width: 50%\">
                    <input type=\"text\" name=\"{{ field.name }}\" value=\"{{ field.default_value }}\"  id=\"{{field.name}}\"
                        {{ (field.readonly==1)? 'readonly=\"readonly\"' : ''}}
                    />
                </li>
                <li class=\"sep\">{{field.suffix}}</li>
            </ul>
        </div>
        {% elseif field.type == \"select\" %}
            {% if field.options is empty %}
            <ul>
                <li style=\"width: 80%\"><blockquote>{% trans 'Please click on \"Edit\" in order to add new select box' %}</blockquote></li>
            </ul>
            {% else %}
                <select name=\"{{ field.name }}\" required=\"required\"   id=\"{{field.name}}\"
                    {{ (field.readonly==1)? 'disabled=\"disabled\"' : ''}}
                    />
                    {% for k,v in field.options %}
                    <option value=\"{{v}}\"
                    {{ (field.default_value == v) ? 'selected' : '' }}>{{k}}</option>
                    {% endfor %}

                </select>
            {% endif %}
        {% elseif field.type == \"checkbox\" %}
            {% if field.options is empty %}
            <ul>
                <li style=\"width: 80%\"><blockquote>{% trans 'Please click on \"Edit\" in order to add new checkbox' %}</blockquote></li>
            </ul>
            {% else %}
                {% for k,v in field.options %}
                <input type=\"checkbox\" name=\"{{field.name}}\" value=\"{{v}}\" id=\"{{k}}_{{v}}\" {% if v in field.default_value %}checked=\"checked\"{% endif %}
                    {{ (field.readonly==1)? 'disabled=\"disabled\"' : ''}}
            />
                <label for=\"{{k}}_{{v}}\">{{k}}</label>
                {% endfor %}
            {% endif %}
        {% elseif field.type == \"radio\" %}
            {% if field.options is empty %}
                <ul>
                    <li style=\"width: 80%\"><blockquote>{% trans 'Please click on \"Edit\" in order to add new radio box' %}</blockquote></li>
                </ul>
            {% else %}
                {% for k,v in field.options %}
                <input type=\"radio\" name=\"{{field.name}}\" value=\"{{v}}\" id=\"{{k}}_{{v}}\" {{ (field.default_value == v) ? 'checked' : '' }}
                    {{ (field.readonly==1)? 'disabled=\"disabled\"' : ''}}
            />
                <label for=\"{{k}}_{{v}}\">{{k}}</label>
                {% endfor %}
            {% endif %}
        {% elseif field.type == \"textarea\" %}
            <div style=\"width: auto; max-width:50%;\">
                <textarea name=\"{{field.name}}\" style=\"width: {{field.options.width}}px; height: {{field.options.height}}px; overflow: scroll;\">{{field.default_value}}</textarea>
            </div>
        {% endif %}
        </div>
        <div class=\"fix\"></div>

    <div class=\"actions\" style=\"right: 0; bottom: 0; padding: 5px; position: absolute;\">
<!--        <a class=\"btn14 pr btnIconLeft\" href=\"#\" style=\"display: none;\"><img src=\"images/icons/dark/preview.png\" alt=\"Preview\" class=\"icon\">Preview</a>-->
        <a class=\"btn14 ed\" href=\"#\"><img src=\"images/icons/dark/pencil.png\" alt=\"Edit\" class=\"icon\"></a>
        <a class=\"btn14 rm\" href=\"#\" data-field-id=\"{{field.id}}\"><img src=\"images/icons/dark/trash.png\" alt=\"\" class=\"icon\"></a>
    </div>
    <div class=\"fix\"></div>

</div>", "mod_formbuilder_preview.phtml", "/var/www/html/bb-modules/Formbuilder/html_admin/mod_formbuilder_preview.phtml");
    }
}
