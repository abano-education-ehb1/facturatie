<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mod_staff_login_history.phtml */
class __TwigTemplate_5ef0e028415af795cfd1c6b58ed0ed3e0d67dbe65b1f1457770b15b4dc888847 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'meta_title' => [$this, 'block_meta_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return $this->loadTemplate(((twig_get_attribute($this->env, $this->source, ($context["request"] ?? null), "ajax", [], "any", false, false, false, 1)) ? ("layout_blank.phtml") : ("layout_default.phtml")), "mod_staff_login_history.phtml", 1);
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["mf"] = $this->macros["mf"] = $this->loadTemplate("macro_functions.phtml", "mod_staff_login_history.phtml", 2)->unwrap();
        // line 4
        $context["active_menu"] = "activity";
        // line 1
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_meta_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Staff members login history";
    }

    // line 6
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "<div class=\"widget\">
    <div class=\"head\"><h5 class=\"iFrames\">";
        // line 8
        echo gettext("Staff members logins history");
        echo "</h5></div>

";
        // line 10
        echo twig_call_macro($macros["mf"], "macro_table_search", [], 10, $context, $this->getSourceContext());
        echo "
<table class=\"tableStatic wide\">
    <thead>
        <tr>
            <td style=\"width: 2%\"><input type=\"checkbox\" class=\"batch-delete-master-checkbox\"/></td>
            <td colspan=\"2\">";
        // line 15
        echo gettext("Admin");
        echo "</td>
            <td>";
        // line 16
        echo gettext("IP");
        echo "</td>
            <td>";
        // line 17
        echo gettext("Country");
        echo "</td>
            <td>";
        // line 18
        echo gettext("Date");
        echo "</td>
            <td style=\"width: 5%\">&nbsp;</td>
        </tr>
    </thead>

    <tbody>
    ";
        // line 24
        $context["history"] = twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "staff_login_history_get_list", [0 => twig_array_merge(["per_page" => 30, "page" => twig_get_attribute($this->env, $this->source, ($context["request"] ?? null), "page", [], "any", false, false, false, 24)], ($context["request"] ?? null))], "method", false, false, false, 24);
        // line 25
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["history"] ?? null), "list", [], "any", false, false, false, 25));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["i"] => $context["event"]) {
            // line 26
            echo "    <tr>
        <td><input type=\"checkbox\" class=\"batch-delete-checkbox\" data-item-id=\"";
            // line 27
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["event"], "id", [], "any", false, false, false, 27), "html", null, true);
            echo "\"/></td>
        <td style=\"width:5%\"><a href=\"";
            // line 28
            echo $this->extensions['Box_TwigExtensions']->twig_bb_admin_link_filter("staff/manage");
            echo "/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["event"], "staff", [], "any", false, false, false, 28), "id", [], "any", false, false, false, 28), "html", null, true);
            echo "\"><img src=\"";
            echo twig_escape_filter($this->env, twig_gravatar_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["event"], "staff", [], "any", false, false, false, 28), "email", [], "any", false, false, false, 28)), "html", null, true);
            echo "?size=20\" alt=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["event"], "staff", [], "any", false, false, false, 28), "email", [], "any", false, false, false, 28), "html", null, true);
            echo "\" /></a></td>
        <td>";
            // line 29
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["event"], "staff", [], "any", false, false, false, 29), "name", [], "any", false, false, false, 29), "html", null, true);
            echo "</td>
        <td>";
            // line 30
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["event"], "ip", [], "any", false, false, false, 30), "html", null, true);
            echo "</td>
        <td>";
            // line 31
            echo twig_escape_filter($this->env, _twig_default_filter($this->extensions['Box_TwigExtensions']->twig_ipcountryname_filter(twig_get_attribute($this->env, $this->source, $context["event"], "ip", [], "any", false, false, false, 31)), "Unknown"), "html", null, true);
            echo "</td>
        <td>";
            // line 32
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["event"], "created_at", [], "any", false, false, false, 32), "Y-m-d H:i"), "html", null, true);
            echo "</td>
        <td class=\"actions\">
            <a class=\"bb-button btn14 bb-rm-tr api-link\" data-api-confirm=\"Are you sure?\" data-api-redirect=\"";
            // line 34
            echo $this->extensions['Box_TwigExtensions']->twig_bb_admin_link_filter("staff/logins");
            echo "\" href=\"";
            echo $this->extensions['Box_TwigExtensions']->twig_bb_client_link_filter("api/admin/staff/login_history_delete", ["id" => twig_get_attribute($this->env, $this->source, $context["event"], "id", [], "any", false, false, false, 34)]);
            echo "\"><img src=\"images/icons/dark/trash.png\" alt=\"\"></a>
        </td>
    </tr>
    ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 38
            echo "    <tr>
        <td colspan=\"7\">
            ";
            // line 40
            echo gettext("The list is empty");
            // line 41
            echo "        </td>
    </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['i'], $context['event'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "    </tbody>
</table>

</div>

";
        // line 49
        $this->loadTemplate("partial_batch_delete.phtml", "mod_staff_login_history.phtml", 49)->display(twig_array_merge($context, ["action" => "admin/staff/batch_delete_logs"]));
        // line 50
        $this->loadTemplate("partial_pagination.phtml", "mod_staff_login_history.phtml", 50)->display(twig_array_merge($context, ["list" => ($context["history"] ?? null), "url" => "staff/logins"]));
        // line 51
        echo "
";
    }

    public function getTemplateName()
    {
        return "mod_staff_login_history.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  177 => 51,  175 => 50,  173 => 49,  166 => 44,  158 => 41,  156 => 40,  152 => 38,  141 => 34,  136 => 32,  132 => 31,  128 => 30,  124 => 29,  114 => 28,  110 => 27,  107 => 26,  101 => 25,  99 => 24,  90 => 18,  86 => 17,  82 => 16,  78 => 15,  70 => 10,  65 => 8,  62 => 7,  58 => 6,  51 => 3,  47 => 1,  45 => 4,  43 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends request.ajax ? \"layout_blank.phtml\" : \"layout_default.phtml\" %}
{% import \"macro_functions.phtml\" as mf %}
{% block meta_title %}Staff members login history{% endblock %}
{% set active_menu = 'activity' %}

{% block content %}
<div class=\"widget\">
    <div class=\"head\"><h5 class=\"iFrames\">{% trans 'Staff members logins history' %}</h5></div>

{{ mf.table_search }}
<table class=\"tableStatic wide\">
    <thead>
        <tr>
            <td style=\"width: 2%\"><input type=\"checkbox\" class=\"batch-delete-master-checkbox\"/></td>
            <td colspan=\"2\">{% trans 'Admin' %}</td>
            <td>{% trans 'IP' %}</td>
            <td>{% trans 'Country' %}</td>
            <td>{% trans 'Date' %}</td>
            <td style=\"width: 5%\">&nbsp;</td>
        </tr>
    </thead>

    <tbody>
    {% set history = admin.staff_login_history_get_list({\"per_page\":30, \"page\":request.page}|merge(request)) %}
    {% for i, event in history.list %}
    <tr>
        <td><input type=\"checkbox\" class=\"batch-delete-checkbox\" data-item-id=\"{{ event.id }}\"/></td>
        <td style=\"width:5%\"><a href=\"{{ 'staff/manage'|alink }}/{{ event.staff.id }}\"><img src=\"{{ event.staff.email|gravatar }}?size=20\" alt=\"{{ event.staff.email }}\" /></a></td>
        <td>{{ event.staff.name }}</td>
        <td>{{ event.ip }}</td>
        <td>{{ event.ip|ipcountryname|default('Unknown') }}</td>
        <td>{{ event.created_at|date('Y-m-d H:i') }}</td>
        <td class=\"actions\">
            <a class=\"bb-button btn14 bb-rm-tr api-link\" data-api-confirm=\"Are you sure?\" data-api-redirect=\"{{'staff/logins'|alink }}\" href=\"{{ 'api/admin/staff/login_history_delete'|link({'id' : event.id}) }}\"><img src=\"images/icons/dark/trash.png\" alt=\"\"></a>
        </td>
    </tr>
    {% else %}
    <tr>
        <td colspan=\"7\">
            {% trans 'The list is empty' %}
        </td>
    </tr>
    {% endfor %}
    </tbody>
</table>

</div>

{% include \"partial_batch_delete.phtml\" with {'action' : 'admin/staff/batch_delete_logs'} %}
{% include \"partial_pagination.phtml\" with {'list': history, 'url':'staff/logins'} %}

{% endblock %}
", "mod_staff_login_history.phtml", "/var/www/html/bb-themes/admin_default/html/mod_staff_login_history.phtml");
    }
}
