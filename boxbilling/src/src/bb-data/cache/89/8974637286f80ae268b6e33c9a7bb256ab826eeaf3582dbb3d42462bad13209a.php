<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mod_formbuilder_field.phtml */
class __TwigTemplate_987f53d0c5e6a4161711f36db0df47d500eb16deeb75fda2ae53f8ffc83e6431 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<form method=\"post\" action=\"\" name=\"field_form";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "id", [], "any", false, false, false, 1), "html", null, true);
        echo "\" class=\"field-form update-field\">
<div class=\"head\"><h5><span class=\"awe-edit\"></span> ";
        // line 2
        echo gettext("Edit field");
        echo "</h5><a href=\"#\" class=\"floatright close-field-form\"><span class=\"ui-icon ui-icon-closethick ui-icon\" ></span></a></div>
<div class=\"element\">

<div class=\"manage highlight\">

<div class=\"rowElem\">

    <label for=\"name\">";
        // line 9
        echo gettext("Name");
        echo "</label>

    <div class=\"formRight moreFields\">
        <ul>
            <li style=\"width: 300px\">
                <input type=\"text\" name=\"name\" value=\"";
        // line 14
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "name", [], "any", false, false, false, 14), "html", null, true);
        echo "\" id=\"name\"/>
            </li>
            <li class=\"sep\">
                ";
        // line 17
        echo gettext("Please keep in mind that field name can not start with number. Special characters will be replaced with underscrore ( _ )");
        // line 18
        echo "            </li>
        </ul>
    </div>

</div>
<div class=\"rowElem\">
<label for=\"label\">";
        // line 24
        echo gettext("Label");
        echo "</label>

    <div class=\"formRight moreFields\">

        <ul>
            <li style=\"width: 300px\">
                <input type=\"text\" name=\"label\" value=\"";
        // line 30
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "label", [], "any", false, false, false, 30), "html", null, true);
        echo "\" id=\"label\"/>
            </li>

        </ul>

    </div>
</div>
    <div class=\"fix\"></div>



";
        // line 41
        if ((((twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "type", [], "any", false, false, false, 41) == "checkbox") || (twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "type", [], "any", false, false, false, 41) == "radio")) || (twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "type", [], "any", false, false, false, 41) == "select"))) {
            // line 42
            echo "<div class=\"rowElem\">
    <label>";
            // line 43
            echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "type", [], "any", false, false, false, 43)), "html", null, true);
            echo " ";
            echo gettext("options");
            echo "</label>

    <div class=\"formRight moreFields\">
        <div class=\"rowElem noborder\">
            <ul>
                <li style=\"width: 200px\">
                    <label>";
            // line 49
            echo gettext("Label");
            echo "</label>
                </li>
                <li class=\"sep\"></li>
                <li style=\"width: 200px\">
                    <label>";
            // line 53
            echo gettext("Value");
            echo "</label>
                </li>
            </ul>
        </div>
        ";
            // line 57
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "options", [], "any", false, false, false, 57));
            foreach ($context['_seq'] as $context["k"] => $context["v"]) {
                // line 58
                echo "        <div class=\"rowElem noborder\">
            <ul>
                <li style=\"width: 200px\">
                    <input type=\"text\" name=\"labels[]\" value=\"";
                // line 61
                echo twig_escape_filter($this->env, $context["k"], "html", null, true);
                echo "\"/>
                </li>
                <li class=\"sep\">=</li>
                <li style=\"width: 200px\">
                    <input type=\"text\" name=\"values[]\" value=\"";
                // line 65
                echo twig_escape_filter($this->env, $context["v"], "html", null, true);
                echo "\"/>
                </li>
            </ul>
        </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['k'], $context['v'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 70
            echo "        <div class=\"rowElem noborder copyfields\">
            <ul>
                <li style=\"width: 200px\">
                    <input type=\"text\" name=\"labels[]\"/>
                </li>
                <li class=\"sep\">=</li>
                <li style=\"width: 200px\">
                    <input type=\"text\" name=\"values[]\"/>
                </li>
            </ul>
        </div>
        <div class=\"rowElem noborder\">
            <ul>
                <li style=\"width: 200px\">
                    <label>
                        <a href=\"#\" class=\"new-field button greyishBtn\" rel=\"";
            // line 85
            echo twig_escape_filter($this->env, ($context["i"] ?? null), "html", null, true);
            echo "\">";
            echo gettext("+ Add new option");
            echo "</a>
                    </label>
                </li>
            </ul>
        </div>
        <div class=\"fix\"></div>
    </div>
    <div class=\"fix\"></div>

</div>
";
        }
        // line 96
        echo "
<div class=\"rowElem\">
    <label>";
        // line 98
        echo gettext("Options");
        echo "</label>

    <div class=\"formRight moreFields\">

                <input type=\"checkbox\" name=\"required\" value=\"1\" id=\"required\" value=\"1\" ";
        // line 102
        echo (((twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "required", [], "any", false, false, false, 102) == 1)) ? ("checked=\"checked\"") : (""));
        echo "/>
                <label for=\"required\">";
        // line 103
        echo gettext("Required");
        echo "</label>

                <input type=\"checkbox\" name=\"hide_label\" value=\"1\" id=\"hide_label\" value=\"1\" ";
        // line 105
        echo (((twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "hide_label", [], "any", false, false, false, 105) == 1)) ? ("checked=\"checked\"") : (""));
        echo "/>
                <label for=\"hide_label\">";
        // line 106
        echo gettext("Hide label");
        echo "</label>

                <input type=\"checkbox\" name=\"readonly\" value=\"1\" id=\"readonly\" value=\"1\" ";
        // line 108
        echo (((twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "readonly", [], "any", false, false, false, 108) == 1)) ? ("checked=\"checked\"") : (""));
        echo "/>
                <label for=\"readonly\">";
        // line 109
        echo gettext("Read only");
        echo "</label>

    </div>
    <div class=\"fix\"></div>

</div>
";
        // line 115
        if ((twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "type", [], "any", false, false, false, 115) == "text")) {
            // line 116
            echo "<div class=\"rowElem\">
    <label>";
            // line 117
            echo gettext("Additional settings");
            echo "</label>


    <div class=\"formRight moreFields\">
        <ul>
            <label for=\"prefix_text\">";
            // line 122
            echo gettext("Prepend text");
            echo "</label>
            <li style=\"width: 100px\">
                <input type=\"text\" name=\"prefix\" value=\"";
            // line 124
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "prefix", [], "any", false, false, false, 124), "html", null, true);
            echo "\" id=\"prefix_text\"/>
            </li>
            <li class=\"sep\"></li>
            <label for=\"suffix_text\">";
            // line 127
            echo gettext("Append text");
            echo "</label>
            <li style=\"width: 100px\">
                <input type=\"text\" name=\"suffix\" value=\"";
            // line 129
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "suffix", [], "any", false, false, false, 129), "html", null, true);
            echo "\" id=\"suffix_text\"/>
            </li>

        </ul>

    </div>
    <div class=\"fix\"></div>

</div>
";
        }
        // line 139
        if ((twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "type", [], "any", false, false, false, 139) == "textarea")) {
            // line 140
            echo "<div class=\"rowElem\">
    <label>";
            // line 141
            echo gettext("Textarea size");
            echo "</label>


    <div class=\"formRight moreFields\">
        <ul>
            <label for=\"textarea-height\">";
            // line 146
            echo gettext("Width");
            echo "</label>
            <li style=\"width: 100px\">
                <input type=\"text\" name=\"textarea_size[]\" value=\"";
            // line 148
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "options", [], "any", false, false, false, 148), "width", [], "any", false, false, false, 148), "html", null, true);
            echo "\" id=\"textarea-width\"/>
                <input type=\"hidden\" name=\"textarea_option[]\" value=\"width\"/>
            </li>
            <li class=\"sep\">";
            // line 151
            echo gettext("px");
            echo "</li>
            <label for=\"textarea-width\">";
            // line 152
            echo gettext("Height");
            echo "</label>
            <li style=\"width: 100px\">
                <input type=\"text\" name=\"textarea_size[]\" value=\"";
            // line 154
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "options", [], "any", false, false, false, 154), "height", [], "any", false, false, false, 154), "html", null, true);
            echo "\" id=\"textarea-height\"/>
                <input type=\"hidden\" name=\"textarea_option[]\" value=\"height\"/>
            </li>
            <li class=\"sep\">";
            // line 157
            echo gettext("px");
            echo "</li>
        </ul>
    </div>
    <div class=\"fix\"></div>

</div>
";
        }
        // line 164
        echo "
<div class=\"rowElem\">
    <label for=\"default_value\">";
        // line 166
        echo gettext("Default value/option");
        echo "</label>

    <div class=\"formRight moreFields\">
        <ul>
            ";
        // line 170
        if ((twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "type", [], "any", false, false, false, 170) == "text")) {
            // line 171
            echo "                    <li class=\"sep\" id=\"prepended_text\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "prefix", [], "any", false, false, false, 171), "html", null, true);
            echo "</li>
                    <li style=\"width: 50%\"><input type=\"text\" name=\"default_value\" value=\"";
            // line 172
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "default_value", [], "any", false, false, false, 172), "html", null, true);
            echo "\"  id=\"default_value\"/></li>
                    <li class=\"sep\" id=\"appended_text\">";
            // line 173
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "suffix", [], "any", false, false, false, 173), "html", null, true);
            echo "</li>
            ";
        } elseif ((twig_get_attribute($this->env, $this->source,         // line 174
($context["field"] ?? null), "type", [], "any", false, false, false, 174) == "checkbox")) {
            // line 175
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "options", [], "any", false, false, false, 175));
            foreach ($context['_seq'] as $context["k"] => $context["v"]) {
                // line 176
                echo "                <input type=\"checkbox\" name=\"default_value[]\" value=\"";
                echo twig_escape_filter($this->env, $context["v"], "html", null, true);
                echo "\" id=\"";
                echo twig_escape_filter($this->env, $context["k"], "html", null, true);
                echo "_";
                echo twig_escape_filter($this->env, $context["v"], "html", null, true);
                echo "\" ";
                if (twig_in_filter($context["v"], twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "default_value", [], "any", false, false, false, 176))) {
                    echo "checked=\"checked\"";
                }
                echo ">
                <label for=\"";
                // line 177
                echo twig_escape_filter($this->env, $context["k"], "html", null, true);
                echo "_";
                echo twig_escape_filter($this->env, $context["v"], "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $context["k"], "html", null, true);
                echo "</label>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['k'], $context['v'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 179
            echo "            ";
        } elseif ((twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "type", [], "any", false, false, false, 179) == "radio")) {
            // line 180
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "options", [], "any", false, false, false, 180));
            foreach ($context['_seq'] as $context["k"] => $context["v"]) {
                // line 181
                echo "                <input type=\"radio\" name=\"default_value\" value=\"";
                echo twig_escape_filter($this->env, $context["v"], "html", null, true);
                echo "\" id=\"";
                echo twig_escape_filter($this->env, $context["k"], "html", null, true);
                echo "_";
                echo twig_escape_filter($this->env, $context["v"], "html", null, true);
                echo "\" ";
                if (($context["v"] == twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "default_value", [], "any", false, false, false, 181))) {
                    echo "checked=\"checked\"";
                }
                echo ">
                <label for=\"";
                // line 182
                echo twig_escape_filter($this->env, $context["k"], "html", null, true);
                echo "_";
                echo twig_escape_filter($this->env, $context["v"], "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $context["k"], "html", null, true);
                echo "</label>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['k'], $context['v'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 184
            echo "            ";
        } elseif ((twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "type", [], "any", false, false, false, 184) == "select")) {
            // line 185
            echo "                <select name=\"default_value\">
                    <option value=\"\">---</option>
                    ";
            // line 187
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "options", [], "any", false, false, false, 187));
            foreach ($context['_seq'] as $context["k"] => $context["v"]) {
                // line 188
                echo "                    <option value=\"";
                echo twig_escape_filter($this->env, $context["v"], "html", null, true);
                echo "\"
                    ";
                // line 189
                echo (((twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "default_value", [], "any", false, false, false, 189) == $context["v"])) ? ("selected") : (""));
                echo ">";
                echo twig_escape_filter($this->env, $context["k"], "html", null, true);
                echo "</option>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['k'], $context['v'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 191
            echo "                </select>
            ";
        } elseif ((twig_get_attribute($this->env, $this->source,         // line 192
($context["field"] ?? null), "type", [], "any", false, false, false, 192) == "textarea")) {
            // line 193
            echo "            <li style=\"width: auto; max-width:500px; overflow: scroll;\">
                <textarea name=\"default_value\" style=\"width: ";
            // line 194
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "options", [], "any", false, false, false, 194), "width", [], "any", false, false, false, 194), "html", null, true);
            echo "px; height: ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "options", [], "any", false, false, false, 194), "height", [], "any", false, false, false, 194), "html", null, true);
            echo "px; \">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "default_value", [], "any", false, false, false, 194), "html", null, true);
            echo "</textarea>
            </li>
            ";
        }
        // line 197
        echo "        </ul>
    </div>

    <div class=\"fix\"></div>
</div>
<div class=\"rowElem\">
    <label for=\"description\">";
        // line 203
        echo gettext("Description");
        echo "</label>

    <div class=\"formRight moreFields\">
        <ul>
            <li style=\"width: 300px\">
                <input type=\"text\" name=\"description\" value=\"";
        // line 208
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "description", [], "any", false, false, false, 208), "html", null, true);
        echo "\" id=\"description\"/>
            </li>
        </ul>
    </div>
    <div class=\"fix\"></div>
</div>

<div class=\"rowElem pull-right\">
    <input type=\"submit\" class=\"button blueBtn save-field-form\" value=\"";
        // line 216
        echo gettext("Save");
        echo "\">
    <div class=\"fix\"></div>
</div>
</div>
</div>

<input type=\"hidden\" name=\"id\" value=\"";
        // line 222
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "id", [], "any", false, false, false, 222), "html", null, true);
        echo "\">
<input type=\"hidden\" name=\"form_id\" value=\"";
        // line 223
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "id", [], "any", false, false, false, 223), "html", null, true);
        echo "\">
<input type=\"hidden\" name=\"type\" value=\"";
        // line 224
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["field"] ?? null), "type", [], "any", false, false, false, 224), "html", null, true);
        echo "\"/>
</form>";
    }

    public function getTemplateName()
    {
        return "mod_formbuilder_field.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  501 => 224,  497 => 223,  493 => 222,  484 => 216,  473 => 208,  465 => 203,  457 => 197,  447 => 194,  444 => 193,  442 => 192,  439 => 191,  429 => 189,  424 => 188,  420 => 187,  416 => 185,  413 => 184,  401 => 182,  388 => 181,  383 => 180,  380 => 179,  368 => 177,  355 => 176,  350 => 175,  348 => 174,  344 => 173,  340 => 172,  335 => 171,  333 => 170,  326 => 166,  322 => 164,  312 => 157,  306 => 154,  301 => 152,  297 => 151,  291 => 148,  286 => 146,  278 => 141,  275 => 140,  273 => 139,  260 => 129,  255 => 127,  249 => 124,  244 => 122,  236 => 117,  233 => 116,  231 => 115,  222 => 109,  218 => 108,  213 => 106,  209 => 105,  204 => 103,  200 => 102,  193 => 98,  189 => 96,  173 => 85,  156 => 70,  145 => 65,  138 => 61,  133 => 58,  129 => 57,  122 => 53,  115 => 49,  104 => 43,  101 => 42,  99 => 41,  85 => 30,  76 => 24,  68 => 18,  66 => 17,  60 => 14,  52 => 9,  42 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<form method=\"post\" action=\"\" name=\"field_form{{ field.id }}\" class=\"field-form update-field\">
<div class=\"head\"><h5><span class=\"awe-edit\"></span> {% trans 'Edit field' %}</h5><a href=\"#\" class=\"floatright close-field-form\"><span class=\"ui-icon ui-icon-closethick ui-icon\" ></span></a></div>
<div class=\"element\">

<div class=\"manage highlight\">

<div class=\"rowElem\">

    <label for=\"name\">{% trans 'Name' %}</label>

    <div class=\"formRight moreFields\">
        <ul>
            <li style=\"width: 300px\">
                <input type=\"text\" name=\"name\" value=\"{{field.name}}\" id=\"name\"/>
            </li>
            <li class=\"sep\">
                {% trans 'Please keep in mind that field name can not start with number. Special characters will be replaced with underscrore ( _ )' %}
            </li>
        </ul>
    </div>

</div>
<div class=\"rowElem\">
<label for=\"label\">{% trans 'Label' %}</label>

    <div class=\"formRight moreFields\">

        <ul>
            <li style=\"width: 300px\">
                <input type=\"text\" name=\"label\" value=\"{{ field.label }}\" id=\"label\"/>
            </li>

        </ul>

    </div>
</div>
    <div class=\"fix\"></div>



{% if field.type == \"checkbox\" or field.type==\"radio\" or field.type==\"select\" %}
<div class=\"rowElem\">
    <label>{{ field.type|capitalize }} {% trans 'options' %}</label>

    <div class=\"formRight moreFields\">
        <div class=\"rowElem noborder\">
            <ul>
                <li style=\"width: 200px\">
                    <label>{% trans 'Label' %}</label>
                </li>
                <li class=\"sep\"></li>
                <li style=\"width: 200px\">
                    <label>{% trans 'Value' %}</label>
                </li>
            </ul>
        </div>
        {% for k,v in field.options %}
        <div class=\"rowElem noborder\">
            <ul>
                <li style=\"width: 200px\">
                    <input type=\"text\" name=\"labels[]\" value=\"{{k}}\"/>
                </li>
                <li class=\"sep\">=</li>
                <li style=\"width: 200px\">
                    <input type=\"text\" name=\"values[]\" value=\"{{v}}\"/>
                </li>
            </ul>
        </div>
        {% endfor %}
        <div class=\"rowElem noborder copyfields\">
            <ul>
                <li style=\"width: 200px\">
                    <input type=\"text\" name=\"labels[]\"/>
                </li>
                <li class=\"sep\">=</li>
                <li style=\"width: 200px\">
                    <input type=\"text\" name=\"values[]\"/>
                </li>
            </ul>
        </div>
        <div class=\"rowElem noborder\">
            <ul>
                <li style=\"width: 200px\">
                    <label>
                        <a href=\"#\" class=\"new-field button greyishBtn\" rel=\"{{i}}\">{% trans '+ Add new option' %}</a>
                    </label>
                </li>
            </ul>
        </div>
        <div class=\"fix\"></div>
    </div>
    <div class=\"fix\"></div>

</div>
{% endif %}

<div class=\"rowElem\">
    <label>{% trans 'Options' %}</label>

    <div class=\"formRight moreFields\">

                <input type=\"checkbox\" name=\"required\" value=\"1\" id=\"required\" value=\"1\" {{ (field.required == 1)? 'checked=\"checked\"' : \"\" }}/>
                <label for=\"required\">{% trans 'Required' %}</label>

                <input type=\"checkbox\" name=\"hide_label\" value=\"1\" id=\"hide_label\" value=\"1\" {{ (field.hide_label == 1)? 'checked=\"checked\"' : \"\" }}/>
                <label for=\"hide_label\">{% trans 'Hide label' %}</label>

                <input type=\"checkbox\" name=\"readonly\" value=\"1\" id=\"readonly\" value=\"1\" {{ (field.readonly == 1)? 'checked=\"checked\"' : \"\" }}/>
                <label for=\"readonly\">{% trans 'Read only' %}</label>

    </div>
    <div class=\"fix\"></div>

</div>
{% if field.type == \"text\" %}
<div class=\"rowElem\">
    <label>{% trans 'Additional settings' %}</label>


    <div class=\"formRight moreFields\">
        <ul>
            <label for=\"prefix_text\">{% trans 'Prepend text' %}</label>
            <li style=\"width: 100px\">
                <input type=\"text\" name=\"prefix\" value=\"{{field.prefix}}\" id=\"prefix_text\"/>
            </li>
            <li class=\"sep\"></li>
            <label for=\"suffix_text\">{% trans 'Append text' %}</label>
            <li style=\"width: 100px\">
                <input type=\"text\" name=\"suffix\" value=\"{{field.suffix}}\" id=\"suffix_text\"/>
            </li>

        </ul>

    </div>
    <div class=\"fix\"></div>

</div>
{% endif %}
{% if field.type == \"textarea\" %}
<div class=\"rowElem\">
    <label>{% trans 'Textarea size' %}</label>


    <div class=\"formRight moreFields\">
        <ul>
            <label for=\"textarea-height\">{% trans 'Width' %}</label>
            <li style=\"width: 100px\">
                <input type=\"text\" name=\"textarea_size[]\" value=\"{{field.options.width}}\" id=\"textarea-width\"/>
                <input type=\"hidden\" name=\"textarea_option[]\" value=\"width\"/>
            </li>
            <li class=\"sep\">{% trans 'px' %}</li>
            <label for=\"textarea-width\">{% trans 'Height' %}</label>
            <li style=\"width: 100px\">
                <input type=\"text\" name=\"textarea_size[]\" value=\"{{field.options.height}}\" id=\"textarea-height\"/>
                <input type=\"hidden\" name=\"textarea_option[]\" value=\"height\"/>
            </li>
            <li class=\"sep\">{% trans 'px' %}</li>
        </ul>
    </div>
    <div class=\"fix\"></div>

</div>
{% endif %}

<div class=\"rowElem\">
    <label for=\"default_value\">{% trans 'Default value/option' %}</label>

    <div class=\"formRight moreFields\">
        <ul>
            {% if field.type == \"text\"%}
                    <li class=\"sep\" id=\"prepended_text\">{{field.prefix}}</li>
                    <li style=\"width: 50%\"><input type=\"text\" name=\"default_value\" value=\"{{ field.default_value }}\"  id=\"default_value\"/></li>
                    <li class=\"sep\" id=\"appended_text\">{{field.suffix}}</li>
            {% elseif field.type == \"checkbox\" %}
            {% for k,v in field.options %}
                <input type=\"checkbox\" name=\"default_value[]\" value=\"{{v}}\" id=\"{{k}}_{{v}}\" {% if v in field.default_value %}checked=\"checked\"{% endif %}>
                <label for=\"{{k}}_{{v}}\">{{k}}</label>
            {% endfor %}
            {% elseif field.type == \"radio\" %}
                {% for k,v in field.options %}
                <input type=\"radio\" name=\"default_value\" value=\"{{v}}\" id=\"{{k}}_{{v}}\" {% if v == field.default_value %}checked=\"checked\"{% endif %}>
                <label for=\"{{k}}_{{v}}\">{{k}}</label>
                {% endfor %}
            {% elseif field.type == \"select\" %}
                <select name=\"default_value\">
                    <option value=\"\">---</option>
                    {% for k,v in field.options %}
                    <option value=\"{{v}}\"
                    {{ (field.default_value == v) ? 'selected' : '' }}>{{k}}</option>
                    {% endfor %}
                </select>
            {% elseif field.type == \"textarea\" %}
            <li style=\"width: auto; max-width:500px; overflow: scroll;\">
                <textarea name=\"default_value\" style=\"width: {{field.options.width}}px; height: {{field.options.height}}px; \">{{ field.default_value}}</textarea>
            </li>
            {% endif %}
        </ul>
    </div>

    <div class=\"fix\"></div>
</div>
<div class=\"rowElem\">
    <label for=\"description\">{% trans 'Description' %}</label>

    <div class=\"formRight moreFields\">
        <ul>
            <li style=\"width: 300px\">
                <input type=\"text\" name=\"description\" value=\"{{ field.description}}\" id=\"description\"/>
            </li>
        </ul>
    </div>
    <div class=\"fix\"></div>
</div>

<div class=\"rowElem pull-right\">
    <input type=\"submit\" class=\"button blueBtn save-field-form\" value=\"{% trans 'Save' %}\">
    <div class=\"fix\"></div>
</div>
</div>
</div>

<input type=\"hidden\" name=\"id\" value=\"{{ field.id }}\">
<input type=\"hidden\" name=\"form_id\" value=\"{{ form.id }}\">
<input type=\"hidden\" name=\"type\" value=\"{{ field.type }}\"/>
</form>", "mod_formbuilder_field.phtml", "/var/www/html/bb-modules/Formbuilder/html_admin/mod_formbuilder_field.phtml");
    }
}
