<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mod_servicecustom_order.phtml */
class __TwigTemplate_927c3cacfcfd2a34061e16350223f9c2bb0ce8252196a9559005d8c33d3fc9c8 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "stock_control", [], "any", false, false, false, 1)) {
            // line 2
            echo "<div class=\"rowElem\">
    <label>";
            // line 3
            echo gettext("Quantity");
            echo ":</label>
    <div class=\"formRight\">
        <input type=\"text\" name=\"quantity\" value=\"1\"/>
    </div>
    <div class=\"fix\"></div>
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "mod_servicecustom_order.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if product.stock_control %}
<div class=\"rowElem\">
    <label>{% trans 'Quantity' %}:</label>
    <div class=\"formRight\">
        <input type=\"text\" name=\"quantity\" value=\"1\"/>
    </div>
    <div class=\"fix\"></div>
</div>
{% endif %}", "mod_servicecustom_order.phtml", "/var/www/html/bb-modules/Servicecustom/html_admin/mod_servicecustom_order.phtml");
    }
}
