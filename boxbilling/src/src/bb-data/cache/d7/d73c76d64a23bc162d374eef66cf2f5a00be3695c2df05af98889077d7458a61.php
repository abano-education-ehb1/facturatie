<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__70e77d73af5a05beffc47774c7a44f7a6cf758ccb6f6252f7ed31b61891f189d */
class __TwigTemplate_3c1643d2ffad95c2ff81d774586ded245f9e793b6d82b0e2a701395cf014a517 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "
";
        // line 2
        ob_start();
        // line 3
        echo "
Hello ";
        // line 4
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["c"] ?? null), "first_name", [], "any", false, false, false, 4), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["c"] ?? null), "last_name", [], "any", false, false, false, 4), "html", null, true);
        echo ",

Your **";
        // line 6
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "title", [], "any", false, false, false, 6), "html", null, true);
        echo "** is now active.

Login to members area: ";
        // line 8
        echo $this->extensions['Box_TwigExtensions']->twig_bb_client_link_filter("login", ["email" => twig_get_attribute($this->env, $this->source, ($context["c"] ?? null), "email", [], "any", false, false, false, 8)]);
        echo "
Manage order: ";
        // line 9
        echo $this->extensions['Box_TwigExtensions']->twig_bb_client_link_filter("order/service/manage");
        echo "/";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "id", [], "any", false, false, false, 9), "html", null, true);
        echo "

";
        // line 11
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["guest"] ?? null), "system_company", [], "any", false, false, false, 11), "signature", [], "any", false, false, false, 11), "html", null, true);
        echo "

";
        $___internal_parse_0_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 2
        echo twig_markdown_filter($this->env, $___internal_parse_0_);
    }

    public function getTemplateName()
    {
        return "__string_template__70e77d73af5a05beffc47774c7a44f7a6cf758ccb6f6252f7ed31b61891f189d";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 2,  68 => 11,  61 => 9,  57 => 8,  52 => 6,  45 => 4,  42 => 3,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("
{% apply markdown %}

Hello {{ c.first_name }} {{ c.last_name }},

Your **{{ order.title }}** is now active.

Login to members area: {{'login'|link({'email' : c.email }) }}
Manage order: {{ 'order/service/manage'|link }}/{{ order.id }}

{{ guest.system_company.signature }}

{% endapply %}
", "__string_template__70e77d73af5a05beffc47774c7a44f7a6cf758ccb6f6252f7ed31b61891f189d", "");
    }
}
