<?php 
return array (
  'debug' => false,
  'salt' => 'f0e708d0afdbfaf8735c65c580eb0a7d',
  'url' => 'http://10.3.56.3:9100/',
  'admin_area_prefix' => '/bb-admin',
  'sef_urls' => true,
  'timezone' => 'UTC',
  'locale' => 'en_US',
  'locale_date_format' => '%A, %d %B %G',
  'locale_time_format' => ' %T',
  'path_data' => '/var/www/html/src/bb-data',
  'path_logs' => '/var/www/html/src/bb-data/log/application.log',
  'log_to_db' => true,
  'db' => 
  array (
    'type' => 'mysql',
    'host' => '10.3.56.3',
    'name' => 'facturatie',
    'user' => 'facturatie',
    'password' => 'rLcp5K4WLEpy7W4hbfmz34c9xHL7DUjH',
  ),
  'twig' => 
  array (
    'debug' => true,
    'auto_reload' => true,
    'cache' => '/var/www/html/src/bb-data/cache',
  ),
  'api' => 
  array (
    'require_referrer_header' => false,
    'allowed_ips' => 
    array (
    ),
    'rate_span' => 3600,
    'rate_limit' => 1000,
  ),
);
