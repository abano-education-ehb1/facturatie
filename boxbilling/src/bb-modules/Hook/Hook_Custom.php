<?php
/**
 * Example product plugin usage
 */

    public static function onAfterOrderActivate(Box_Event $event)
    {
        $order = $event->getSubject();
        $plugin = $order->Product->plugin;
        if($plugin == 'MyPlugin') {
            // init plugin class
            // do something with plugin on order activation action
        }
    }

    public static function onAfterOrderRenew(Box_Event $event)
    {

    }

    public static function onAfterOrderSuspend(Box_Event $event)
    {

    }

    public static function onAfterOrderUnsuspend(Box_Event $event)
    {

    }

    public static function onAfterOrderCancel(Box_Event $event)
    {

    }

    public static function onAfterOrderUncancel(Box_Event $event)
    {

    }

    public static function onAfterOrderDelete(Box_Event $event)
    {
        
    }
